<?php

/**
 * bootstrap.php
 * This is the main file to include when making a new site using Kojin.
 * For more details see the sample site.
 */

// Ensure PHP 5.4.0
if (version_compare(phpversion(), "5.4.0") == -1) {
	echo "Kojin requires PHP 5.4.0, detected version: " . phpversion() . "\n";
	exit(1);
}

// Utility constants
defined("DS") || define("DS", DIRECTORY_SEPARATOR);

// Directory constants
define("KOJIN_ROOT", realpath(dirname(__FILE__)));
define("KOJIN_SRC", KOJIN_ROOT . DS . "src" . DS . "Kojin");
define("KOJINEXT_SRC", KOJIN_ROOT . DS . "src" . DS."KojinExt");

// Time
date_default_timezone_set("UTC");

// Require class loader
require_once(KOJIN_SRC . DS . "ClassLoader.php");

// Register default Kojin autoloader
// We have to do it here instead of the convenience method on Kojin, since Kojin has dependencies
spl_autoload_register(array(
	new \Kojin\ClassLoader("Kojin", KOJIN_SRC),
	"load"
));

// Require Kojin
require_once(KOJIN_SRC . DS . "Kojin.php");

// Error handling
set_error_handler(array("\Kojin\Kojin", "__error_handler"), 0x7fffffff);
error_reporting(0x7fffffff);

// Register ClassLoader for bundled extensions
\Kojin\Kojin::registerAutoloader(new \Kojin\ClassLoader("KojinExt", \Kojin\Kojin::getExtensionDir()->getPath()));
