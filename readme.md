# Kojin
Kojin is a simple PHP framework designed for easy implementation of smaller websites.
It was designed as an alternative to other popular CMSs and is intented for people who are much more comfortable with text editors than fancy GUIs.

# Changelog

## 0.2.0 (in development)

* Router refactor.
* Extension load refactor.
* Multi-pass PageRenderer.
* Varsub extension pulling variables from config files.

## 0.1.0

* Initial alpha release.
