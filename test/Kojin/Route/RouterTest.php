<?php

namespace Kojin\Route;

use Kojin\Kojin;
use Kojin\Content\Site;

/**
 * Test for Router.
 */
class SiteTest extends \PHPUnit_Framework_TestCase {
	/**
	 * Path elements used for router path detection.
	 * These paths are treated and stored in $realPaths below.
	 */
	public static $paths = array(
		array("/somewhere/kojin/sample",  "/foo/bar"),
		array("somewhere/kojin/sample",   "/foo/bar",  "/foo/bar"),
		array("somewhere/kojin/sample/",  "foo/bar",   "/foo/bar"),
		array("/somewhere/kojin/sample/", "/foo/bar/", "/foo/bar")
	);

	/**
	 * Combined paths actually used in the tests below.
	 * 0: The root of the "site"
	 * 1: The full path
	 * 2: the desired detected path inside the root
	 */
	public static $realPaths;

	/**
	 * Clean router.
	 * @var \Kojin\Router
	 */
	public $router;

	public static function setUpBeforeClass() {
		// Prepare paths
		static::$realPaths = array();
		foreach (static::$paths as $path) {
			$p = array(
				$path[0],
				$path[0] . $path[1],
				$path[1]
			);
			if (count($path) === 3) {
				$p[2] = $path[2];
			}
			static::$realPaths[] = $p;
		}
	}

	public function setUp() {
		// @todo simplify, this should not be this complicated
		$kojin = new Kojin(SITE_ROOT);
		$this->router = $kojin->getRouter();
		$route = new FileTypeRoute();
		$route->addFileType("md");
		$this->router->addRoute("markdown", $route, "markdown");
	}

	/**
	 * Tests sanitizeUrl().
	 */
	public function testSanitizeUrl() {
		$urls = array(
			// Simple slashes
			array("/", "/"),
			array("/", "///"),
			// Subpaths
			array("/foo/bar", "//foo//bar//"),
			// Relative paths
			array("/", ".."),
			array("/", "/.."),
			array("/", "../"),
			array("/", "/../"),
			array("/", "/../.."),
			array("/", "/foo/../../"),
			array("/bar", "/../foo/../bar"),
			// Legal characters
			array("/~+-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz/%22[]!@", "/~+-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz/%22[]!@"),
			// Illegal characters
			array("/foo/bar", "/foo/`¬|\\'}{/bar/")
		);
		foreach ($urls as $i => $u) {
			$this->assertEquals($u[0], Router::sanitizeUrl($u[1]), "i:$i");
		}
	}

	/**
	 * Tests detectRelativePath().
	 */
	public function testDetectRelativePath() {
		foreach (static::$realPaths as $i => $path) {
			$this->assertEquals(
				$path[2],
				$this->router->detectRelativePath($path[0], $path[1]),
				$i . "::" . print_r($path, true)
			);
		}
	}

	/**
	 * Tests getRelativePath().
	 */
	public function testGetRelativePath() {
		foreach (static::$realPaths as $i => $path) {
			$router = new Router($path[0]);
			$this->assertEquals(
				$path[2],
				$router->getRelativePath($path[1]),
				$i . "::" . print_r($path, true)
			);
		}
	}

	/**
	 * Tests creating a router and detecting the path in the PHP environment.
	 */
	public function testEnvironmentDetection() {
		$orig = $_SERVER;
		$tests = array(
			array(
				"PHP_SELF" => "/kojin/sample/public/index.php",
				"REQUEST_URI" => "/kojin/sample/public/foo/bar/",
				"path" => "/foo/bar"
			),
			array(
				"PHP_SELF" => "/kojin/sample/public/index.php",
				"REQUEST_URI" => "/kojin/sample/public/",
				"path" => "/"
			)
		);
		foreach ($tests as $i => $test) {
			// Get path
			$path = $test["path"];
			unset($test["path"]);
			// Set environment
			foreach ($test as $key => $val) {
				$_SERVER[$key] = $val;
			}
			$router = Router::createRouter();
			$this->assertEquals($path, $router->getRelativePath());
		}
	}
	
	/**
	 * Tests routeContent(). 
	 */
	public function testRouteContent() {
		// Prepare site
		$contentPath = $this->router->getKojin()->getSite()->getContentDir()->getPath();

		// Route to root, which will be a page since there's an index file
		$page = $this->router->route("/");
		$this->assertInstanceOf("\Kojin\Content\Page", $page);
		$this->assertEquals($contentPath.DS."index.md", $page->getFile()->getPath());
		$this->assertEquals("/index.md", $page->getUrlPath());
		$this->assertEquals("/", $page->getFolder()->getUrlPath());

		// Route to /foo/bar page
		$page = $this->router->route("/foo/bar");
		$this->assertInstanceOf("\Kojin\Content\Page", $page);
		$this->assertEquals($contentPath.DS."foo".DS."bar.md", $page->getFile()->getPath());
		$this->assertEquals("/foo/bar.md", $page->getUrlPath());
		$this->assertEquals("/foo", $page->getFolder()->getUrlPath());

		// Route to page by full filename
		$page = $this->router->route("/index.md");
		$this->assertInstanceOf("\Kojin\Content\Page", $page);
		$this->assertEquals($contentPath.DS."index.md", $page->getFile()->getPath());
		$this->assertEquals("/index.md", $page->getUrlPath());
		$this->assertEquals("/", $page->getFolder()->getUrlPath());

		// Route to page by sub-folder index
		$page = $this->router->route("/foo");
		$this->assertInstanceOf("\Kojin\Content\Page", $page);
		$this->assertEquals($contentPath.DS."foo".DS."index.md", $page->getFile()->getPath());
		$this->assertEquals("/foo/index.md", $page->getUrlPath());
		$this->assertEquals("/foo", $page->getFolder()->getUrlPath());
	}
}
