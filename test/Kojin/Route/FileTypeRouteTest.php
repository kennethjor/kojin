<?php

namespace Kojin\Route;

use \Kojin\Kojin;
use \Kojin\Content\Site;

/**
 * Test for FileTypeRoute.
 */
class FileTypeRouteTest extends \PHPUnit_Framework_TestCase {
	public $kojin;

	/**
	 * Clean router.
	 * @var \Kojin\Route\FileTypeRoute
	 */
	public $route;

	public function setUp() {
		$this->kojin = new Kojin(SITE_ROOT);
		$this->route = new FileTypeRoute();
		$this->route->setKojin($this->kojin);
		$this->route->addFileType("md", "text/x-markdown");
	}

	/**
	 * Custom assert to check route result routes to a specific file.
	 */
	public function assertFileRoute($file, $result) {
		$file = $this->kojin->getSite()->getContentDir()->getFile($file);

		$this->assertInternalType("array", $result);
		// Mimetype
		$this->assertArrayHasKey("mimetype", $result);
		$this->assertEquals($result["mimetype"], "text/x-markdown");
		// File
		$this->assertArrayHasKey("file", $result);
		$supplied = $result["file"];
		$this->assertInstanceOf("Kojin\Fs\File", $supplied);
		$this->assertEquals($file->getPath(), $supplied->getPath());
	}

	public function testRouteNonExistant() {
		$this->assertFalse($this->route->execute("doesntexist"));
	}

	public function testRouteExist() {
		// Direct
		$result = $this->route->execute("/index.md");
		$this->assertFileRoute("index.md", $result);
		// Indirect
		$result = $this->route->execute("/index");
		$this->assertFileRoute("index.md", $result);
	}

	public function testRouteWrongFileType() {
		// Direct
		$this->assertFalse($this->route->execute("/htmlpage.html"));
		// Indirect
		$this->assertFalse($this->route->execute("/htmlpage"));
	}

	public function testRouteFolder() {
		$result = $this->route->execute("/");
		$this->assertFileRoute("index.md", $result);
	}
}
