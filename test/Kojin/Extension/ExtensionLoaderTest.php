<?php

namespace Kojin\Extension;

use Kojin\Kojin;
use Kojin\ClassLoader;
use Kojin\Config\Config;
use Kojin\Content\Site;
use Kojin\Route\Router;

/**
 * Test for ExtensionLoader.
 */
class ExtensionLoaderTest extends \PHPUnit_Framework_TestCase {
	/** @var Kojin\Kojin */
	public $kojin;
	/** @var Kojin\ExtensionLoader */
	public $loader;

	/**
	 * Kojin's extension directory.
	 * @var?
	 */
	public $extensionDir;

	public function setUp() {
		$this->kojin = new Kojin(SITE_ROOT);
		$this->loader = $this->kojin->getExtensionLoader();

		// This should not be necesarry!
		//$router = Router::createRouter();
		//$router->setSite($kojin->getSite());
		//$kojin->setRouter($router);

		//$this->loader = new ExtensionLoader();
		//$this->loader->setSite($kojin->getSite());
		//$this->extensionDir = Kojin::getExtensionDir();
		//$this->loader->addExtensionDir($this->extensionDir);
	}

	/**
	 * Tests getClassFile().
	 */
	public function testGetClassFile() {
		// \Kojin\Kojin
		$class = ExtensionLoader::getFileClass(KOJIN_SRC . DS . "Kojin.php");
		$this->assertEquals(array(
			"class" => "Kojin\Kojin",
			"namespace" => "Kojin"
		), $class);
		
		// \Kojin\Config\YamlConfig
		$class = ExtensionLoader::getFileClass(KOJIN_SRC . DS . "Config" . DS . "YamlConfig.php");
		$this->assertEquals(array(
			"class" => "Kojin\Config\YamlConfig",
			"namespace" => "Kojin\Config"
		), $class);
	}

	/**
	 * Tests getAvailableExtensions().
	 */
	public function testGetAvailableExtensions() {
		$available = $this->loader->getAvailableExtensions();
		$this->assertArrayHasKey("html", $available);
		$this->assertArrayHasKey("image", $available);
		$this->assertArrayHasKey("markdown", $available);
		$this->assertArrayHasKey("url", $available);
		$this->assertArrayHasKey("varsub", $available);

		$extensionDir = Kojin::getExtensionDir()->getPath();
		$this->assertEquals(array(
			"dir" => $extensionDir . DS . "Markdown",
			"mainFile" => $extensionDir . DS . "Markdown" . DS . "MarkdownExtension.php"
		), $available["markdown"]);
	}

	/**
	 * Tests isExtensionEnabled().
	 */
	public function testIsExtensionEnabled() {
		// No extension config
		$this->loader->setExtensionConfig(new Config());
		$this->assertFalse($this->loader->isExtensionEnabled("markdown"));

		// Enable
		$extensionConfig = array(
			"html" => array(
				"enabled" => true
			),
			"markdown" => array(
				"enabled" => true
			)
		);
		$this->loader->setExtensionConfig(new Config($extensionConfig));
		$this->assertTrue($this->loader->isExtensionEnabled("markdown"));
		$this->assertTrue($this->loader->isExtensionEnabled("html"));
		$this->assertFalse($this->loader->isExtensionEnabled("doesntexist"));

		// Disable
		$extensionConfig["markdown"]["enabled"] = false;
		$this->loader->setExtensionConfig(new Config($extensionConfig));
		$this->assertFalse($this->loader->isExtensionEnabled("markdown"));
		$this->assertTrue($this->loader->isExtensionEnabled("html"));
		$this->assertFalse($this->loader->isExtensionEnabled("doesntexist"));

		// Remove extension config
		unset($extensionConfig["markdown"]);
		$this->loader->setExtensionConfig(new Config($extensionConfig));
		$this->assertFalse($this->loader->isExtensionEnabled("markdown"));
		$this->assertTrue($this->loader->isExtensionEnabled("html"));
		$this->assertFalse($this->loader->isExtensionEnabled("doesntexist"));
	}

	/**
	 * Tests loadExtensions().
	 */
	public function testLoadExtensions() {
		$this->assertEquals(0, count($this->kojin->getRouter()->getRoutes()));
		$this->loader->loadExtensions();
		$this->assertNotEquals(0, count($this->kojin->getRouter()->getRoutes()));
	}

	/**
	 * Tests loadExtension() for the bundled Markdown extension.
	 */
	public function testLoadExtension() {
		$html = $this->loader->loadExtension("html");
		$this->assertInstanceOf("KojinExt\Html\HtmlExtension", $html);

		$image = $this->loader->loadExtension("image");
		$this->assertInstanceOf("KojinExt\Image\ImageExtension", $image);

		$markdown = $this->loader->loadExtension("markdown");
		$this->assertInstanceOf("KojinExt\Markdown\MarkdownExtension", $markdown);

		$url = $this->loader->loadExtension("url");
		$this->assertInstanceOf("KojinExt\Url\UrlExtension", $url);

		$varsub = $this->loader->loadExtension("varsub");
		$this->assertInstanceOf("KojinExt\Varsub\VarsubExtension", $varsub);
	}

	/**
	 * Tests loadExtension() on non-existant extension.
	 * @expectedException \Kojin\Extension\ExtensionNotFoundException
	 */
	public function testLoadExtensionNotFound() {
		$this->loader->loadExtension("doesntexist");
	}

	/**
	 * Tests loadExtension() returns the same object if called twice.
	 */
	public function testLoadExtensionTwice() {
		$e1 = $this->loader->loadExtension("markdown");
		$e2 = $this->loader->loadExtension("markdown");
		$this->assertSame($e1, $e2);
	}

	/**
	 * Tests loadExtension() on a disabled extension.
	 * @expectedException \Kojin\Extension\ExtensionNotEnabledException
	 */
	public function testLoadExtensionDisabled() {
		$this->loader->setExtensionConfig(new Config(array(
		"html" => array(
			"enabled" => false
		))));
		$this->loader->loadExtension("html");
	}

	/**
	 * Tests getEnabledExtensions() with empty config.
	 */
	// public function testGetEnabledExtensions() {
	// 	$enabled = $this->loader->getEnabledExtensions();
	// 	$this->assertEquals(0, count($enabled));
	// }

	/**
	 * Tests getEnabledExtensions with an extension explicity enabled.
	 */
	// public function testGetEnabledExtensionsEnabled() {
	// 	$this->loader->setExtensionConfig(new Config(array(
	// 		"markdown" => array(
	// 			"enabled" => true
	// 		)
	// 	)));
	// 	$enabled = $this->loader->getEnabledExtensions();
	// 	$this->assertArrayHasKey("markdown", $enabled, print_r($enabled, true));
	// }

	/**
	 * Tests getEnabledExtensions with an extension explicity disabled.
	 */
	// public function testGetEnabledExtensionsDisabled() {
	// 	$this->loader->setExtensionConfig(new Config(array(
	// 		"markdown" => array(
	// 			"enabled" => false
	// 		)
	// 	)));
	// 	$enabled = $this->loader->getEnabledExtensions();
	// 	$this->assertFalse(array_key_exists("markdown", $enabled));
	// }

	/**
	 * Tests getEnabledExtensions with an unknown extension configured.
	 * @expectedException \Kojin\Extension\ExtensionNotFoundException
	 */
	// public function testGetEnabledExtensionsUnknown() {
	// 	$this->loader->setExtensionConfig(new Config(array(
	// 		"markdown" => array(
	// 			"enabled" => true
	// 		),
	// 		"doesntexist" => array(
	// 			"enabled" => true
	// 		)
	// 	)));
	// 	$enabled = $this->loader->getEnabledExtensions();
	// }
	
	/**
	 * Tests getPageLoaders(). 
	 */
	// public function testGetPageLoaders() {
	// 	$this->loader->setExtensionConfig(new Config(array(
	// 		"markdown" => array(
	// 			"enabled" => true
	// 		),
	// 		"html" => array(
	// 			"enabled" => false
	// 		)
	// 	)));
	// 	$contentLoaders = $this->loader->getPageLoaders();
	// 	$this->assertEquals(1, count($contentLoaders));
	// 	$this->arrayHasKey("markdown", $contentLoaders, print_r($contentLoaders, true));
	// }
	
	/**
	 * Tests getPageProcessors(). 
	 */
	// public function testGetPageProcessors() {
	// 	$this->loader->setExtensionConfig(new Config(array(
	// 		"markdown" => array(
	// 			"enabled" => true
	// 		),
	// 		"url" => array(
	// 			"enabled" => true
	// 		),
	// 		"varsub" => array(
	// 			"enabled" => false
	// 		)
	// 	)));
	// 	$pageProcessors = $this->loader->getPageProcessors();
	// 	$this->assertEquals(1, count($pageProcessors));
	// 	$this->assertArrayHasKey("url", $pageProcessors, print_r($pageProcessors, true));
	// }
	


	/**
	 * Tests redundant autoloaders have not been registered.
	 * NOTE: This test MUST be last!
	 */
	public function testRedundantAutoloaders() {
		$autoloaders = spl_autoload_functions();
		// Filter class loaders
		$extensionLoaders = array();
		foreach ($autoloaders as $loader) {
			if (is_array($loader) && $loader[0] instanceof ClassLoader) {
				$classLoader = $loader[0];
				if (substr($classLoader->getNamespace(), 0, 9) === "KojinExt\\") {
					$extensionLoaders[] = $classLoader;
				}
			}
		}
		// Deduce namespaces
		$namespaces = array();
		foreach ($extensionLoaders as $loader) {
			$space = $loader->getNamespace();
			if (in_array($space, $namespaces)) {
				$this->fail("Extension loader for the namespace \"$space\" already registered :: " . print_r($extensionLoaders, true));
			}
			$namespaces[] = $space;
		}
	}
}
