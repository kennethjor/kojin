<?php

namespace Kojin\Render;

use \DOMDocument;

/**
 * Tests for Html.
 */
class HtmlTest extends \PHPUnit_Framework_TestCase {
	public $source = "<!DOCTYPE html><html><head></head><body></body></html>";
	/** @var \Kojin\Render\Html */
	public $html;
	
	public function setUp() {
		$this->html = new Html($this->source);
	}
	
	/**
	 * Tests getDom().
	 */
	public function testGetDom() {
		$dom = $this->html->getDom();
		$this->assertInstanceOf("\DOMDocument", $dom);
		$this->AssertEquals(2, $dom->childNodes->length);
		// Doctype
		$doctype = $dom->doctype;
		$this->assertEquals("html", $doctype->name);
		// <html/>
		$html = $dom->childNodes->item(1);
		$this->assertEquals("html", $html->tagName);
		$this->assertEquals(2, $html->childNodes->length);
		
		// A second call to getDom() should result in the same object
		$this->assertSame($dom, $this->html->getDom());
	}
	
	/**
	 * Tests getSource().
	 */
	public function testGetSource() {
		$this->assertEquals($this->source, $this->html->getSource());
	}
	
	/**
	 * Tests setSource().
	 */
	public function testSetSource() {
		// Setting the source to the same as before should not reconstruct the DOM.
		$dom = $this->html->getDom();
		$this->html->setSource($this->source);
		$this->assertSame($dom, $this->html->getDom());
		// Setting the source to something different should
		$source = "<!DOCTYPE html><html class=\"new\"><head></head><body></body></html>";
		$this->html->setSource($source);
		$this->assertEquals($source, $this->html->getSource());
		$this->assertNotSame($dom, $this->html->getDom());
		// Assert different source on DOM
		$dom = $this->html->getDom();
		$this->assertEquals("new", $dom->childNodes->item(1)->attributes->getNamedItem("class")->value);
	}
	
	/**
	 * Tests setDom() and getSource().
	 */
	public function testSetDomGetSource() {
		$source = "<!DOCTYPE html>\n<html class=\"new\"><head></head><body></body></html>\n";
		$dom = new DOMDocument("1.0", "utf8");
		$dom->loadHTML($source);
		// Set DOM and assert change
		$oldDom = $this->html->getDom();
		$this->html->setDom($dom);
		$this->assertNotSame($oldDom, $this->html->getDom());
		$this->assertSame($dom, $this->html->getDom());
		// Assert source
		$this->assertEquals($source, $this->html->getSource());
		// Setting the DOM to the same should not result in a new source
	}
	
	/**
	 * Tests any alterations to the DOM propagates to the source.
	 */
	public function testAlterDomPropagate() {
		$dom = $this->html->getDom();
		$body = $dom->getElementsByTagName("body")->item(0);
		$body->appendChild($dom->createElement("div"));
		// DOM should propagate without calling setDom()
		$source = "<!DOCTYPE html>\n<html><head></head><body><div></div></body></html>\n";
		$this->assertEquals($source, $this->html->getSource());
	}
	
	/**
	 * Tests xpath().
	 */
	public function testXpath() {
		$source = <<<HTML
<!DOCTYPE html>
<html>
	<body>
		<div class="foo bar">something</div>
		<div class="foo">else</div>
		<span class="bar">other</span>
		<span class="foobar">other</span>
	</body>
</html>
HTML;
		$html = new Html($source);
		// Select the html element
		$result = $html->xpath("/html");
		$this->assertInstanceOf("\DOMNodeList", $result);
		$this->assertEquals(1, $result->length);
		$this->assertEquals("html", $result->item(0)->tagName);
		// Select the two divs
		$result = $html->xpath("//div");
		$this->assertInstanceOf("\DOMNodeList", $result);
		$this->assertEquals(2, $result->length);
		$this->assertEquals("div", $result->item(0)->tagName);
		$this->assertEquals("foo bar", $result->item(0)->getAttributeNode("class")->value);
		$this->assertEquals("div", $result->item(1)->tagName);
		$this->assertEquals("foo", $result->item(1)->getAttributeNode("class")->value);
		// Select the elements with bar classes - *sigh*
		$result = $html->xpath("//*[@class and contains(concat(' ',@class,' '), ' bar ')]");
		$this->assertInstanceOf("\DOMNodeList", $result);
		$this->assertEquals(2, $result->length);
		$this->assertEquals("div", $result->item(0)->tagName);
		$this->assertEquals("span", $result->item(1)->tagName);
	}
}
