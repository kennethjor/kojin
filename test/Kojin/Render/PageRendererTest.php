<?php

namespace Kojin\Render;

use Kojin\Kojin;
use Kojin\Content\Site;
use Kojin\Content\Page;
use Kojin\Fs\File;
use Kojin\Route\Router;

/**
 * Tests for PageRenderer.
 */
class PageRendererTest extends \PHPUnit_Framework_TestCase {
	public $cleanPage;
	public $markdownPage;
	public $renderer;
	
	public function setUp() {
		$kojin = new Kojin(SITE_ROOT);
		$loader = $kojin->getExtensionLoader();
		$site = $kojin->getSite();
		$siteContent = $site->getContentDir()->getPath();

		$this->cleanPage = $site->getPage("/foo/testpages/clean");
		$this->cleanPage->setFile(new File($siteContent.DS."foo".DS."testpages".DS."clean.html"));
		$this->cleanPage->setPageLoader($loader->loadExtension("html"));
		
		$this->markdownPage = $site->getPage("/foo/testpages/markdown");
		$this->markdownPage->setFile(new File($siteContent.DS."foo".DS."testpages".DS."markdown.md"));
		$this->markdownPage->setPageLoader($loader->loadExtension("markdown"));
		
		$this->renderer = new PageRenderer();
		$this->renderer->setKojin($kojin);
	}
	
	public function assertPreConditions() {
		$this->assertEquals("/foo/testpages", $this->cleanPage->getFolder()->getUrlPath());
		$this->assertEquals("/foo/testpages", $this->markdownPage->getFolder()->getUrlPath());
		
		$this->assertEquals("simple.html", $this->cleanPage->getLayout()->getFile()->getBaseName());
		$this->assertEquals("simple.html", $this->markdownPage->getLayout()->getFile()->getBaseName());
	}
	
	/**
	 * Tests renderPage(). 
	 */
	public function testRenderPage() {
		$expected = "<!DOCTYPE html>\n<html><body>content</body></html>\n";
		$this->assertEquals($expected, $this->renderer->renderPage($this->cleanPage));
		
		$expected = "<!DOCTYPE html>\n<html><body><p>content <em>bold</em></p>\n</body></html>\n";
		$this->assertEquals($expected, $this->renderer->renderPage($this->markdownPage));
	}
}
