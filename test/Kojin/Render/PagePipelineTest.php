<?php

namespace Kojin\Render;

use Kojin\Kojin;
use Kojin\Content\Page;
use Kojin\Extension\PageProcessor;

/**
 * Tests for PagePipeline.
 */
class PagePipelineTest extends \PHPUnit_Framework_TestCase {
	public $pipeline;
	public $processor;
	public $page;
	
	public function setUp() {
		$this->page = new Page("/");
		$this->pipeline = new PagePipeline();
		$this->processor = new TestPageProcessor();
		$this->pipeline->addPageProcessor("test", $this->processor);
	}
	
	/**
	 * Tests processPage().
	 */
	public function testProcessPage() {
		// Assert
		$this->assertEquals("x", $this->pipeline->processPage($this->page, ""));
		// Pipeline should execute twice because a change took place
		$this->assertEquals(2, $this->processor->invokations);
	}
}

/**
 * Test PageProcessor.
 * Simply adds the string "x" to the html.
 */
class TestPageProcessor implements PageProcessor {
	public $invokations = 0;
	
	public function processPage(Page $page, $html) {
		$this->invokations++;
		return "x";
	}
}
