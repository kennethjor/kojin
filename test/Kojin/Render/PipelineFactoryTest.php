<?php

namespace Kojin\Render;

use Kojin\Kojin;
use Kojin\Config\Config;

/**
 * Test for PipelineFactory.
 */
class PipelineFactoryTest extends \PHPUnit_Framework_TestCase {
	public $loader;
	public $factory;

	public function setUp() {
		$kojin = new Kojin(SITE_ROOT);
		$this->loader = $kojin->getExtensionLoader();
		$this->loader->setExtensionConfig(new Config(array(
			"url" => array("enabled" => true),
			"varsub" => array("enabled" => false)
		)));
		$this->factory = new PipelineFactory($this->loader);
	}
	
	/**
	 * Tests createPagePipeline(). 
	 */
	public function testCreatePagePipeline() {
		$pipeline = $this->factory->createPagePipeline();
		$this->assertInstanceOf("\Kojin\Render\PagePipeline", $pipeline);
		$processors = $pipeline->getPageProcessors();
		$this->assertEquals(1, count($processors));
		$this->assertArrayHasKey("url", $processors, print_r($processors, true));
	}
}
