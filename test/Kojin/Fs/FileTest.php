<?php

namespace Kojin\Fs;

/**
 * Test for File.
 */
class FileTest extends \PHPUnit_Framework_TestCase {
	// Exists and created with absolute link
	public $realFile;
	public $realPath;
	// Exists and created with relative link - location is the same as realFile
	public $relativePath;
	public $relativeFile;
	// Doesn't exist
	public $fakeFile;
	public $fakePath;
	
	public function setUp() {
		$this->realPath = SITE_ROOT . DS . "site.yaml";
		$this->realFile = new File($this->realPath);
		
		$this->relativePath = SITE_ROOT . DS . "public" . DS . ".." . DS . "site.yaml";
		$this->relativeFile = new File($this->relativePath);
		
		$this->fakePath = "foobar";
		$this->fakeFile = new File($this->fakePath);
	}
	
	/**
	 * Tests exists().
	 */
	public function testExists() {
		$this->assertTrue($this->realFile->exists());
		$this->assertTrue($this->relativeFile->exists());
		$this->assertFalse($this->fakeFile->exists());
	}
	
	/**
	 * Tests getRealPath().
	 */
	public function testGetRealPath() {
		$this->assertEquals($this->realPath, $this->realFile->getRealPath());
		$this->assertEquals($this->realPath, $this->relativeFile->getRealPath());
		$this->assertFalse($this->fakeFile->getRealPath());
	}
	
	/**
	 * Tests getDir().
	 */
	public function testGetDir() {
		$dir = $this->realFile->getDir();
		$this->assertInstanceOf("Kojin\Fs\Directory", $dir);
		$this->assertEquals(dirname($this->realPath), $dir->getPath());
	}
	
	/**
	 * Tests getExtension(). 
	 */
	public function testGetExtension() {
		$this->assertEquals("yaml", $this->realFile->getExtension());
		$this->assertEquals("yaml", $this->relativeFile->getExtension());
		$this->assertEquals("", $this->fakeFile->getExtension());
	}
	
	/**
	 * Tests getContents(). 
	 */
	public function testGetContents() {
		$this->assertEquals(file_get_contents($this->realPath), $this->realFile->getContents());
		$this->assertEquals(file_get_contents($this->relativePath), $this->relativeFile->getContents());
	}
	
	/**
	 * Tests putContents().
	 */
	public function testPutContents() {
		$tmp = Directory::createTempDir("Kojin-FileTest-".__LINE__);
		$file = $tmp->getFile("test");
		$file->putContents("foo");
		$this->assertEquals("foo", file_get_contents($file->getPath()));
		$tmp->removeRecursive();
	}
	
	/**
	 * Tests getContents() with a non-existant file.
	 * @expectedException \Kojin\Fs\FsException
	 */
	public function testGetContentsMissingFile() {
		$this->fakeFile->getContents();
	}
	
	/**
	 * Tests touch() and remove().
	 */
	public function testTouchRemove() {
		// Get file
		$file = new File(sys_get_temp_dir() . DS . str_replace("\\", "_", __CLASS__) . "-" . rand());
		// Assert non-existance
		$this->assertFalse($file->exists());
		// Touch twice, both should return false
		$this->assertTrue($file->touch());
		$this->assertTrue($file->touch());
		// Assert existance
		$this->assertTrue($file->exists());
		// Remove
		$this->assertTrue($file->remove());
		// Assert non-existance
		$this->assertFalse($file->exists());
	}

	/**
	 * Tests getSha1Hash().
	 */
	public function testGetSha1Hash() {
		$hash = "4a72ee1cdab35fdb2192ad90702a670bbfda62ab";
		$this->assertEquals($hash, $this->realFile->getSha1Hash());
	}
}
