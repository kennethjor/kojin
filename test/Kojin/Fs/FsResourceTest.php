<?php

namespace Kojin\Fs;

/**
 * Basic tests for FsResources.
 */
class FsResourceTest extends \PHPUnit_Framework_TestCase {
	public $classes = array(
		"\Kojin\Fs\Directory",
		"\Kojin\Fs\File"
	);
	
	/**
	 * Tests getPath().
	 * @todo duplicated in other, should be common
	 */
	public function testGetPath() {
		$paths = array(
			"/",
			"/foo",
			"foo/bar",
			"foo/../bar"
		);
		foreach ($this->classes as $class) {
			foreach ($paths as $path) {
				$path = str_replace("/", DS, $path);
				$msg = "$class :: $path";
				$res = new $class($path);
				$this->assertEquals($path, $res->getPath(), $msg);
			}
		}
	}
	
	/**
	 * Tests getAbsolutePath(). 
	 */
	public function testGetAbsolutePath() {
		$paths = array(
			// No relative path
			array("a/b/c/d",      "a/b/c/d"),
			// Middle relative parts should be removed
			array("a/b/../c/d",   "a/c/d"),
			// Initial relative parts should be preserved
			array("../a/b",       "../a/b"),
			array("../a/../../b", "../../b")
		);
		foreach ($this->classes as $class) {
			foreach ($paths as $path) {
				$path[0] = str_replace("/", DS, $path[0]);
				$path[1] = str_replace("/", DS, $path[1]);
				$msg = "$class :: {$path[0]}";
				$res = new $class($path[0]);
				$this->assertEquals($path[1], $res->getAbsolutePath(), $msg);
			}
		}
	}
	
	/**
	 * Tests getBaseName().
	 */
	public function testGetBaseName() {
		$paths = array(
			array("a/b/c/d",      "d"),
			array("a/b/../c/d",   "d"),
			array("../a/b",       "b"),
			array("../a/../../b", "b"),
			array("../a/../..",   "..")
		);
		foreach ($this->classes as $class) {
			foreach ($paths as $path) {
				$path[0] = str_replace("/", DS, $path[0]);
				$path[1] = str_replace("/", DS, $path[1]);
				$msg = "$class :: {$path[0]}";
				$res = new $class($path[0]);
				$this->assertEquals($path[1], $res->getBasename(), $msg);
			}
		}
	}
	
	/**
	 * Tests whether setPath() auto-sanitizes paths corretly. 
	 */
	public function testSetPathSanitize() {
		foreach ($this->classes as $class) {
			$res = new $class("/foo//bar/");
			$this->assertEquals("/foo/bar", $res->getPath());
		}
	}
}
