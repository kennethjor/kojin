<?php

namespace Kojin\Fs;

/**
 * Test for Directory.
 */
class DirectoryTest extends \PHPUnit_Framework_TestCase {
	// Exists and created with absolute link
	public $realPath;
	public $realDir;
	// Exists and created with relative link - location is the same as realFile
	public $relativePath;
	public $relativeDir;
	// Doesn't exist
	public $fakePath;
	public $fakeDir;
	
	public function setUp() {
		$this->realPath = SITE_ROOT;
		$this->realDir = new Directory($this->realPath);
		
		$this->relativePath = SITE_ROOT . DS . "public" . DS . "..";
		$this->relativeDir = new Directory($this->relativePath);
		
		$this->fakePath = "foobar";
		$this->fakeDir = new Directory($this->fakePath);
	}
	
	/**
	 * Tests exists().
	 */
	public function testExists() {
		$this->assertTrue($this->realDir->exists());
		$this->assertTrue($this->relativeDir->exists());
		$this->assertFalse($this->fakeDir->exists());
	}
	
	/**
	 * Tests getRealPath().
	 * @todo duplicated in other, should be common
	 */
	public function testGetRealPath() {
		$this->assertEquals($this->realPath, $this->realDir->getRealPath());
		$this->assertEquals($this->realPath, $this->relativeDir->getRealPath());
		$this->assertFalse($this->fakeDir->getRealPath());
	}
	
	/**
	 * Tests getFile(). 
	 */
	public function testGetFile() {
		// Existing file
		$file = $this->realDir->getFile("site.yaml");
		$this->AssertInstanceOf("\Kojin\Fs\File", $file);
		$this->assertEquals($this->realDir->getPath().DS."site.yaml", $file->getPath());
		$this->assertTrue($file->exists());
		
		// Non-existing file
		$file = $this->realDir->getFile("doesnt.exist");
		$this->AssertInstanceOf("\Kojin\Fs\File", $file);
		$this->assertEquals($this->realDir->getPath().DS."doesnt.exist", $file->getPath());
		$this->assertFalse($file->exists());
	}
	
	/**
	 * Tests getDir().
	 */
	public function testGetDir() {
		// Existing file
		$dir = $this->realDir->getdir("content");
		$this->AssertInstanceOf("\Kojin\Fs\Directory", $dir);
		$this->assertEquals($this->realDir->getPath().DS."content", $dir->getPath());
		$this->assertTrue($dir->exists());
		
		// Non-existing file
		$dir = $this->realDir->getDir("doesntexist");
		$this->AssertInstanceOf("\Kojin\Fs\Directory", $dir);
		$this->assertEquals($this->realDir->getPath().DS."doesntexist", $dir->getPath());
		$this->assertFalse($dir->exists());
	}
	
	/**
	 * Tests create() and remove().
	 */
	public function testCreateRemove() {
		// Create new non-existant directory
		$dir = new Directory(sys_get_temp_dir() . DS . str_replace("\\", "_", __CLASS__) . "-" . rand());
		$path = $dir->getPath();
		$this->assertFalse($dir->exists(), $path);
		// Create it twice, both should return true, the second call being noop
		$this->assertTrue($dir->create(), $path);
		$this->assertTrue($dir->create(), $path);
		// Assert existance
		$this->assertTrue($dir->exists(), $path);
		$this->assertTrue(is_dir($path), $path);
		// Remove
		$this->assertTrue($dir->remove(), $path);
		// Assert non-existance
		$this->assertFalse($dir->exists(), $path);
		$this->assertFalse(is_dir($path), $path);
	}
	
	/**
	 * Tests getCreate().
	 */
	public function testGetCreate() {
		$path = sys_get_temp_dir() . DS . str_replace("\\", "_", __CLASS__) . "-" . rand();
		$dir = Directory::getCreate($path);
		$this->assertEquals($path, $dir->getPath(), $path);
		$this->assertTrue($dir->exists(), $path);
		$dir->remove();
	}
	
	/**
	 * Tests createTempDir().
	 */
	public function testCreateTempDir() {
		// Create simple
		$tmp = sys_get_temp_dir();
		$dir = Directory::createTempDir();
		$path = $dir->getPath();
		$this->assertEquals($tmp, substr($dir->getPath(), 0, strlen($tmp)), $path);
		$this->assertTrue($dir->exists(), $dir->getPath());
		$dir->remove();
		
		// Create with prefix
		$prefix = "Kojin-DirectoryTest-".__LINE__;
		$dir = Directory::createTempDir($prefix);
		$path = $dir->getPath();
		$this->assertEquals($tmp, substr($dir->getPath(), 0, strlen($tmp)), $path);
		$this->assertTrue(strpos($dir->getPath(), DS.$prefix."-") !== false, $path);
		$this->assertTrue($dir->exists(), $path);
		$dir->remove();
	}
	
	/**
	 * Tests getContents().
	 */
	public function testGetContents() {
		// Create directory and files
		$tmp = Directory::createTempDir("Kojin-DirectoryTest-".__LINE__);
		$dir = $tmp->getDir("dir");
		$dir->create();
		$file = $tmp->getFile("file");
		$file->touch();
		// Get contents
		$contents = $tmp->getContents();
		$this->assertInternalType("array", $contents);
		$this->assertEquals(2, count($contents), print_r($contents, true));
		foreach ($contents as $c) {
			if ($c instanceof Directory) {
				$this->assertEquals($dir->getPath(), $c->getPath());
			}
			if ($c instanceof File) {
				$this->assertEquals($file->getPath(), $c->getPath());
			}
		}
		// Clean up
		$file->remove();
		$dir->remove();
		$tmp->remove();
	}
	
	/**
	 * Tests removeRecusive().
	 */
	public function testRemoveRecursive() {
		$n = 10;
		// Create directories
		$dir = Directory::createTempDir("KojinTest-".__LINE__);
		$subdir = Directory::getCreate($dir->getPath() . DS . "sub");
		$this->assertTrue($dir->exists());
		$this->assertTrue($subdir->exists());
		// Create files
		$files = array();
		for ($i=0 ; $i<$n ; $i++) {
			// Dir
			$file = new File($dir->getPath() . DS . $i);
			$file->touch();
			$this->assertTrue($file->exists());
			$files[] = $file;
			// Sub dir
			$file = new File($subdir->getPath() . DS . $i);
			$file->touch();
			$this->assertTrue($file->exists());
			$files[] = $file;
		}
		$this->assertTrue($dir->removeRecursive());
		// Assert removal
		foreach ($files as $file) {
			$this->assertFalse($file->exists(), $file->getPath());
		}
		$this->assertFalse($dir->exists(), $dir->getPath());
		$this->assertFalse($subdir->exists(), $subdir->getPath());
	}
}
