<?php

namespace Kojin\Cache;

use Kojin\Fs\Directory;

/**
 * Test for FileCacheObject.
 */
class FileCacheObjectTest extends \PHPUnit_Framework_TestCase {
	public $dir;
	public $cached;
	public $hash = "001122";
	
	public function setUp() {
		$this->dir = Directory::createTempDir("Kojin-FileCacheObjectTest-".__LINE__);
		$this->cached = new FileCacheObject($this->dir, $this->hash);
	}
	
	public function tearDown() {
		$this->dir->removeRecursive();
	}
	
	/**
	 * Tests getHash().
	 */
	public function testGetHash() {
		$this->assertEquals($this->hash, $this->cached->getHash());
	}
	
	/**
	 * Test getFile() and asserts file isn't created immediately.
	 */
	public function testGetFile() {
		$file = $this->cached->getFile();
		$this->assertInstanceOf("\Kojin\Fs\File", $file);
		$this->assertFalse($file->exists());
		$this->assertEquals($this->dir->getPath().DS."00".DS."11".DS."22", $file->getPath());
	}
	
	/**
	 * Tests getBasePath().
	 */
	public function testgetBasePath() {
		$this->assertEquals(DS."00".DS."11".DS."22", $this->cached->getBasePath());
	}
	
	/**
	 * Tests getBaseUrl().
	 */
	public function testgetBaseUrl() {
		$this->assertEquals("/00/11/22", $this->cached->getBaseUrl());
	}
	
	/**
	 * Tests put() and get().
	 */
	public function testPutGet() {
		$content = array("foo" => "bar");
		$this->assertTrue($this->cached->put($content));
		$this->assertTrue($this->cached->getFile()->exists());
		$this->assertEquals($content, $this->cached->get());
	}
	
	/**
	 * Tests setPrefix().
	 */
	public function testSetPrefix() {
		$this->cached->setPrefix("prefix-");
		$this->assertEquals("/00/11/prefix-22", $this->cached->getBaseUrl());
		$this->assertEquals(DS."00".DS."11".DS."prefix-22", $this->cached->getBasePath());
	}
	
	/**
	 * Tests setSuffix().
	 */
	public function testSetSuffix() {
		$this->cached->setSuffix(".suffix");
		$this->assertEquals("/00/11/22.suffix", $this->cached->getBaseUrl());
		$this->assertEquals(DS."00".DS."11".DS."22.suffix", $this->cached->getBasePath());
	}
}
