<?php

namespace Kojin\Cache;

use Kojin\Fs\Directory;

/**
 * Test for FileCache.
 */
class FileCacheTest extends \PHPUnit_Framework_TestCase {
	public $dir;
	public $cache;
	
	public function setUp() {
		$this->dir = Directory::createTempDir("Kojin-FileCacheTest-".__LINE__);
		$this->cache = new FileCache($this->dir);
	}
	
	public function tearDown() {
		$this->dir->removeRecursive();
	}
	
	/**
	 * Tests generateHash() with simple vars.
	 */
	public function testGenerateHash() {
		// Two differnet strings
		$hash1 = $this->cache->generateHash("foo");
		$hash2 = $this->cache->generateHash("bar");
		$this->assertNotEquals($hash1, $hash2);
		// Repeat string
		$hash3 = $this->cache->generateHash("foo");
		$this->assertEquals($hash1, $hash3);
		// Different data types should return different ids
		$this->assertNotEquals(
			$this->cache->generateHash(1),
			$this->cache->generateHash(1.0)
		);
		$this->assertNotEquals(
			$this->cache->generateHash(1),
			$this->cache->generateHash("1")
		);
	}
	
	/**
	 * Tests generateHash() with arrays.
	 */
	public function testGenerateHashArrays() {
		// The order of array elements should not matter
		$hash1 = $this->cache->generateHash(array(
			"foo" => 1,
			"bar" => 2
		));
		$hash2 = $this->cache->generateHash(array(
			"bar" => 2,
			"foo" => 1
		));
		$this->assertEquals($hash1, $hash2);
		// However a different array should generate a different id
		$hash3 = $this->cache->generateHash(array(
			"asd" => 1,
			"bar" => 2
		));
		$this->assertNotEquals($hash1, $hash3);
	}
	
	/**
	 * Tests put() and get().
	 */
	public function testPutGet() {
		$id = "foobar";
		$content = array("foo" => "testObject", "bar" => "flobedifloop");
		$path = $this->cache->getBasePath($this->cache->generateHash($id));
		// Assert object does not exist
		$this->assertNull($this->cache->get($id));
		// Save
		$cached = $this->cache->put($id, $content);
		$this->assertInstanceOf("\Kojin\Cache\FileCacheObject", $cached);
		$file = $cached->getFile();
		$this->assertInstanceOf("\Kojin\Fs\File", $file);
		$this->assertTrue($file->exists());
		$this->assertNull($cached->getUrl());
		// Fetch directly from file
		$this->assertEquals($content, unserialize($file->getContents()));
		// Fetch from object
		$this->assertEquals($content, $cached->get());
		// Fetch from cache
		$this->assertEquals($content, $this->cache->get($id));
	}
	
	/**
	 * Tests setRootUrl() and getRootUrl().
	 */
	public function testSetGetRootUrl() {
		$this->assertNull($this->cache->getRootUrl());
		$this->cache->setRootUrl("/root");
		$this->assertEquals("/root", $this->cache->getRootUrl());
	}
	
	/**
	 * Tests put() with setRootUrl().
	 */
	public function testPutUrl() {
		$this->cache->setRootUrl("/cache");
		$cached = $this->cache->put("x", "y");
		$expected = "/cache/4c/3a/d613bf7d49cecebabe36592057611e7926835a42476800325f5749961d84ff8be92b";
		$this->assertEquals($expected, $cached->getUrl());
	}
	
	/**
	 * Tests put() with prefix.
	 */
	public function testPutPrefix() {
		$this->cache->setRootUrl("/cache");
		$cached = $this->cache->put("x", "y", "prefix-");
		$expected = "/4c/3a/prefix-d613bf7d49cecebabe36592057611e7926835a42476800325f5749961d84ff8be92b";
		$this->assertEquals("/cache".$expected, $cached->getUrl());
		$this->assertEquals($this->dir->getPath() . str_replace("/", DS, $expected), $cached->getFile()->getPath());
		$this->assertEquals("y", $this->cache->get("x", "prefix-"));
	}
	
	/**
	 * Tests put() with suffix.
	 */
	public function testPutSuffix() {
		$this->cache->setRootUrl("/cache");
		$cached = $this->cache->put("x", "y", null, ".suffix");
		$expected = "/4c/3a/d613bf7d49cecebabe36592057611e7926835a42476800325f5749961d84ff8be92b.suffix";
		$this->assertEquals("/cache".$expected, $cached->getUrl());
		$this->assertEquals($this->dir->getPath() . str_replace("/", DS, $expected), $cached->getFile()->getPath());
		$this->assertEquals("y", $this->cache->get("x", null, ".suffix"));
	}
	
	/**
	 * Tests put() with prefix and suffix.
	 */
	public function testPutPrefixSuffix() {
		$this->cache->setRootUrl("/cache");
		$cached = $this->cache->put("x", "y", "prefix-", ".suffix");
		$expected = "/4c/3a/prefix-d613bf7d49cecebabe36592057611e7926835a42476800325f5749961d84ff8be92b.suffix";
		$this->assertEquals("/cache".$expected, $cached->getUrl());
		$this->assertEquals($this->dir->getPath() . str_replace("/", DS, $expected), $cached->getFile()->getPath());
		$this->assertEquals("y", $this->cache->get("x", "prefix-", ".suffix"));
	}
}
