<?php

namespace Kojin;

/**
 * Test for Kojin.
 */
class KojinTest extends \PHPUnit_Framework_TestCase {
	/** @var \Kojin\Kojin */
	public $kojin;

	public function setUp() {
		$this->kojin = new Kojin(SITE_ROOT);
	}

	/**
	 * Tests getSite().
	 */
	public function testGetSite() {
		$site = $this->kojin->getSite();
		$this->assertInstanceOf("\Kojin\Content\Site", $site);
		$this->assertSame($this->kojin, $site->getKojin());
		$this->assertEquals(SITE_ROOT, $this->kojin->getSite()->getDir()->getPath());
	}

	/**
	 * Tests getExtensionLoader().
	 */
	public function testGetExtensionLoader() {
		$loader = $this->kojin->getExtensionLoader();
		$this->assertInstanceOf("\Kojin\Extension\ExtensionLoader", $loader);
		$this->assertSame($this->kojin, $loader->getKojin());
		//$extensions = $loader->getAvailableExtensions();
		//$this->assertArrayHasKey("markdown", $extensions);
	}

	/**
	 * Tests getRouter().
	 */
	public function testGetRouter() {
		$router = $this->kojin->getRouter();
		$this->assertInstanceOf("\Kojin\Route\Router", $router);
		$this->assertSame($this->kojin, $router->getKojin());
	}
}
