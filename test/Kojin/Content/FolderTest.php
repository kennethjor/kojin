<?php

namespace Kojin\Content;

/**
 * Test for Page.
 */
class FolderTest extends \PHPUnit_Framework_TestCase {
	public $site;
	public $folder;

	public function setUp() {
		$this->site = new Site(SITE_ROOT);
		$this->folder = $this->site->getFolder("/foo/testpages");
	}

	/**
	 * Tests getSite().
	 */
	public function testGetSite() {
		$this->assertSame($this->site, $this->folder->getSite());
	}
	
	/**
	 * Tests getFolder(). 
	 */
	public function testGetFolder() {
		$parent = $this->folder->getFolder();
		$this->assertEquals("/foo", $parent->getUrlPath());
	}

	/**
	 * Tests getConfig().
	 */
	public function testGetConfig() {
		$config = $this->folder->getConfig();
		$this->assertEquals("testpages", $config->title);
		$this->assertEquals("simple.html", $config->layout);
		$this->assertNull($config->sitefoo);
	}

	/**
	 * Tests getLayout().
	 */
	public function testGetLayout() {
		// Set directly in config
		$layout = $this->folder->getLayout();
		$this->assertEquals("simple.html", $this->folder->getConfig()->layout);
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertEquals("simple.html", $layout->getFile()->getBaseName());

		// Inherit from site
		$folder = $this->site->getFolder("/foo");
		$layout = $folder->getLayout();
		$this->assertNull($folder->getConfig()->layout);
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertEquals("main.html", $layout->getFile()->getBaseName());
	}
}
