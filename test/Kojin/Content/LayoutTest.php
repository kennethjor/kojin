<?php

namespace Kojin\Content;

/**
 * Test for Layout.
 */
class LayoutTest extends \PHPUnit_Framework_TestCase {
	/** @var \Kojin\Content\Site */
	public $site;
	/** @var \Kojin\Content\Layout */
	public $layout;

	public function setUp() {
		$this->site = new Site(SITE_ROOT);
		$this->layout = $this->site->getLayout("main.html");
	}

	/**
	 * Tests getFile().
	 */
	public function testGetFile() {
		$this->assertEquals($this->site->getLayoutDir()->getPath() . DS . "main.html", $this->layout->getFile()->getPath());
	}

	/**
	 * Tests getSource().
	 */
	public function testGetSource() {
		$expected = file_get_contents($this->layout->getFile()->getPath());
		$supplied = $this->layout->getSource();
		$this->assertEquals($expected, $supplied);
	}
}
