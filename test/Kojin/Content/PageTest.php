<?php

namespace Kojin\Content;

/**
 * Test for Page.
 */
class PageTest extends \PHPUnit_Framework_TestCase {
	/** @var \Kojin\Content\Site */
	public $site;
	/** @var \Kojin\Content\Page */
	public $rootPage;
	/** @var \Kojin\Content\Page */
	public $fooBarPage;
	/** @var \Kojin\Content\Page */
	public $htmlPage;
	/** @var \Kojin\Content\Page */
	public $cleanPage;
	/** @var \Kojin\Content\Page */
	public $configPage;

	public function setUp() {
		$this->site = new Site(SITE_ROOT);
		$this->rootPage = $this->site->getPage("/");
		$this->fooBarPage = $this->site->getPage("/foo/bar");
		$this->htmlPage = $this->site->getPage("/htmlpage");
		$this->cleanPage = $this->site->getPage("/foo/testpages/clean");
		$this->configPage = $this->site->getPage("/varsub/test");
	}

	/**
	 * Tests getSite().
	 */
	public function testGetSite() {
		$this->assertSame($this->site, $this->rootPage->getSite());
		$this->assertSame($this->site, $this->fooBarPage->getSite());
		$this->assertSame($this->site, $this->htmlPage->getSite());
	}

	/**
	 * Tests getLayout().
	 */
	public function testGetLayout() {
		// Inherit from site
		$layout = $this->rootPage->getLayout();
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertNull($this->rootPage->getConfig()->layout);
		$this->assertEquals("main.html", $layout->getFile()->getBaseName());

		$layout = $this->fooBarPage->getLayout();
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertNull($this->fooBarPage->getConfig()->layout);
		$this->assertEquals("main.html", $layout->getFile()->getBaseName());

		$layout = $this->htmlPage->getLayout();
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertNull($this->htmlPage->getConfig()->layout);
		$this->assertEquals("main.html", $layout->getFile()->getBaseName());

		// Inherit from folder
		$layout = $this->cleanPage->getLayout();
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertNull($this->cleanPage->getConfig()->layout);
		$this->assertEquals("simple.html", $layout->getFile()->getBaseName());

		// Set directly in page config
		$layout = $this->configPage->getLayout();
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->assertEquals("simple.html", $this->configPage->getConfig()->layout);
		$this->assertEquals("simple.html", $layout->getFile()->getBaseName());
	}
	
	/**
	 * Tests getFolder(). 
	 */
	public function testGetFolder() {
		$this->assertEquals("/", $this->rootPage->getFolder()->getUrlPath());
		$this->assertEquals("/foo", $this->fooBarPage->getFolder()->getUrlPath());
		$this->assertEquals("/", $this->htmlPage->getFolder()->getUrlPath());
		$this->assertEquals("/foo/testpages", $this->cleanPage->getFolder()->getUrlPath());
	}

	/**
	 * Tests getConfig().
	 */
	public function testGetConfig() {
		$config = $this->configPage->getConfig();
		$this->assertEquals("test page", $config->title);
		$this->assertEquals("2000-01-01", $config->date);
		$this->assertEquals("bar", $config->foo);
		$this->assertEquals("qwerty", $config->qwe->rty);
		$this->assertNull($config->sitefoo);
	}
}
