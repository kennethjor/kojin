<?php

namespace Kojin\Content;

/**
 * Test for Site.
 */
class SiteTest extends \PHPUnit_Framework_TestCase {
	public $siteTitle = "Kojin Test Site";
	
	public $belowRootPaths = array(
		"..",
		"/..",
		"../",
		"/../",
		"/foo/../../../"
	);

	/** @var \Kojin\Content\Site */
	public $site;

	public function setUp() {
		$this->site = new Site(SITE_ROOT);
	}

	public function assertPreConditions() {
		$this->assertInstanceOf("\Kojin\Content\Site", $this->site);
	}

	/**
	 * Tests Site construction with incorrect directory.
	 * @expectedException \Kojin\Content\NotFoundException
	 */
	public function testConstructNotfound() {
		new Site(SITE_ROOT . "notthere");
	}

	/**
	 * Tests getDir().
	 */
	public function testGetDir() {
		$this->AssertEquals(SITE_ROOT, $this->site->getDir()->getPath());
	}

	/**
	 * Tests getContentDir().
	 */
	public function testGetContentDir() {
		$this->AssertEquals(SITE_ROOT.DS."content", $this->site->getContentDir()->getPath());
	}

	/**
	 * Tests getConfig().
	 */
	public function testGetConfig() {
		$config = $this->site->getConfig();
		$this->assertInstanceOf("\Kojin\Config\Config", $config);
	}

	/**
	 * Tests getTitle().
	 */
	public function testGetTitle() {
		$this->assertEquals($this->siteTitle, $this->site->getTitle());
	}

	/**
	 * Tests getPage().
	 */
	public function testGetPage() {
		// Root
		$page = $this->site->getPage("/");
		$this->assertInstanceOf("\Kojin\Content\Page", $page);
		$this->AssertSame($this->site, $page->getSite());
		$this->assertEquals("/", $page->getUrlPath());
		$this->assertEquals("/", $page->getFolder()->getUrlPath());

		// Subpage
		$page = $this->site->getPage("/foo/bar");
		$this->assertInstanceOf("\Kojin\Content\Page", $page);
		$this->AssertSame($this->site, $page->getSite());
		$this->assertEquals("/foo/bar", $page->getUrlPath());
		$this->assertEquals("/foo", $page->getFolder()->getUrlPath());
	}

	/**
	 * Tests getLayout().
	 */
	public function testGetLayout() {
		$layout = $this->site->getLayout("main.html");
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->AssertSame($this->site, $layout->getSite());
		
		$layoutDirPath = $this->site->getLayoutDir()->getPath();
		$this->assertEquals($layoutDirPath.DS."main.html", $layout->getFile()->getPath());
		$this->assertEquals($layoutDirPath, $layout->getFile()->getDir()->getPath());
	}

	/**
	 * Tests getLayout() with configured layout.
	 */
	public function testGetLayoutConfigured() {
		$layout = $this->site->getLayout();
		$this->assertInstanceOf("\Kojin\Content\Layout", $layout);
		$this->AssertSame($this->site, $layout->getSite());
	}
	
	/**
	 * Tests getFolder(). 
	 */
	public function testGetFolder() {
		// Root
		$rootFolder = $this->site->getFolder("/");
		$this->assertInstanceOf("\Kojin\Content\Folder", $rootFolder);
		$this->assertEquals("/", $rootFolder->getUrlPath());
		$this->assertSame($this->site, $rootFolder->getSite());
		$this->assertNull($rootFolder->getFolder());
		$this->assertEquals($this->site->getContentDir()->getPath(), $rootFolder->getDir()->getPath());
		// Foo folder
		$fooFolder = $this->site->getFolder("/foo");
		$this->assertInstanceOf("\Kojin\Content\Folder", $fooFolder);
		$this->assertEquals("/foo", $fooFolder->getUrlPath());
		$this->assertSame($this->site, $fooFolder->getSite());
		$this->assertEquals($rootFolder->getUrlPath(), $fooFolder->getFolder()->getUrlPath());
		$this->assertEquals($this->site->getContentDir()->getPath().DS."foo", $fooFolder->getDir()->getPath());
	}
	
	/**
	 * Tests getPage() below root.
	 */
	public function testGetPageBelowRoot() {
		foreach ($this->belowRootPaths as $path) {
			$page = $this->site->getPage("../");
			$this->assertEquals("/", $page->getUrlPath());
		}
	}
	
	/**
	 * Tests getFolder() below root.
	 */
	public function testGetFolderBelowRoot() {
		foreach ($this->belowRootPaths as $path) {
			$folder = $this->site->getFolder($path);
			$this->assertEquals("/", $folder->getUrlPath());
		}
	}
	
	/**
	 * Tests getPublicCache().
	 */
	public function testGetPublicCache() {
		$cache = $this->site->getPublicCache();
		$this->assertInstanceOf("\Kojin\Cache\FileCache", $cache);
		$this->assertEquals(SITE_ROOT.DS."public".DS."cache", $cache->getDir()->getPath());
		$this->assertEquals($this->site->getRootUrl()."/cache", $cache->getRootUrl());
		//$cache->getDir()->remove();
	}
}
