<?php

namespace Kojin\Config;

/**
 * Test for Config.
 */
class ConfigTest extends \PHPUnit_Framework_TestCase {
	public $data = array(
		"foo" => "bar",
		"animals" => array(
			"cat",
			"dog",
			"elephant",
			"bear"
		),
		"people" => array(
			"jack" => "husband",
			"jill" => "wife"
		)
	);

	/** @var \Kojin\Config\Config */
	public $config;

	public function setUp() {
		$this->config = new Config($this->data);
	}

	public function assertPreConditions() {
		$this->assertInstanceOf("\Kojin\Config\Config", $this->config);
	}

	/**
	 * Tests simple variable resolution.
	 */
	public function testSimpleVars() {
		$this->assertEquals($this->data["foo"], $this->config->foo);
		$this->assertEquals($this->data["people"]["jack"], $this->config->people->jack);
		$this->assertEquals($this->data["people"]["jill"], $this->config->people->jill);
		$this->assertNull($this->config->doesnotexist);
		$this->assertTrue(isset($this->config->foo));
		$this->assertFalse(isset($this->config->doesnotexist));
	}

	/**
	 * Tests ArrayAccess functionality.
	 */
	public function testArrayAccess() {
		$this->assertEquals($this->data["foo"], $this->config["foo"]);
		$this->assertEquals($this->data["animals"][0], $this->config->animals[0]);
		$this->assertEquals($this->data["animals"][3], $this->config->animals[3]);
		$this->assertEquals($this->data["people"]["jack"], $this->config["people"]["jack"]);
		$this->assertEquals($this->data["people"]["jill"], $this->config["people"]["jill"]);
		$this->assertTrue(isset($this->config["foo"]));
		$this->assertFalse(isset($this->config["doesnotexist"]));
	}

	/**
	 * Tests Countable functionality.
	 */
	public function testCountable() {
		$this->assertEquals(count($this->data), count($this->config));
	}

	/**
	 * Tests IteratorAggregate functionality.
	 */
	public function testIteratorAggregate() {
		$this->assertInstanceOf("\ArrayIterator", $this->config->getIterator());

		$n = 0;
		foreach ($this->config as $key => $val) {
			$n++;
		}
		$this->assertEquals(count($this->data), $n);
	}

	/**
	 * Tests toArray().
	 */
	public function testToArray() {
		$this->assertEquals($this->data, $this->config->toArray());
	}

	/**
	 * Tests getKeys().
	 */
	public function testGetKeys() {
		$this->assertEquals(array("foo", "animals", "people"), $this->config->getKeys());
		$this->assertEquals(array(0, 1, 2, 3), $this->config->animals->getKeys());
		$this->assertEquals(array("jack", "jill"), $this->config->people->getKeys());
	}

	/**
	 * Tests isAssociative().
	 */
	public function testIsAssociative() {
		$config = new Config(array(
			"a" => 1,
			"b" => 2
		));
		$this->assertTrue($config->isAssociative());
		$this->assertFalse($config->isNumeric());
	}

	/**
	 * Tests isNumeric().
	 */
	public function testIsNumeric() {
		$config = new Config(array(
			1,
			2
		));
		$this->assertTrue($config->isNumeric());
		$this->assertFalse($config->isAssociative());
	}

	/**
	 * Tests isEmpty().
	 */
	public function testIsEmpty() {
		$this->assertFalse($this->config->isEmpty());
		$empty = new Config();
		$this->assertTrue($empty->isEmpty());
	}

	/**
	 * Tests construction with mixing associative and numeric elements.
	 * @expectedException \Kojin\Config\ConfigException
	 */
	public function testConstructMixNumericAssociative() {
		new Config(array(
			"foo",
			1,
			"bar" => "bar"
		));
	}

	/**
	 * Tests extend().
	 */
	public function testExtend() {
		$base = array(
			"title" => "baseTitle",
			"foo" => "foo",
			"fruits" => array("apple", "orange"),
			"extensions" => array(
				"markdown" => array(
					"enabled" => true
				),
				"html" => array(
					"enabled" => true
				)
			)
		);
		$override = array(
			"title" => "overrideTitle",
			"bar" => "bar",
			"fruits" => array("apple", "pear"),
			"extensions" => array(
				"markdown" => array(
					"enabled" => false
				),
				"xslt" => array(
					"enabled" => true
				)
			)
		);
		$config = new Config($base);
		$extended = $config->extend(new Config($override));
		$msg = print_r($extended->toArray(), true);

		$this->assertEquals($override["title"], $extended->title, $msg);
		$this->assertEquals($base["foo"], $extended->foo, $msg);
		$this->assertEquals($override["bar"], $extended->bar, $msg);
		// Fruits
		$this->assertEquals(3, count($extended->fruits->toArray()), $msg);
		$this->assertContains("apple", $extended->fruits->toArray(), $msg);
		$this->assertContains("orange", $extended->fruits->toArray(), $msg);
		$this->assertContains("pear", $extended->fruits->toArray(), $msg);
		// Extensions
		$this->assertFalse($extended->extensions->markdown->enabled, $msg);
		$this->assertTrue($extended->extensions->html->enabled, $msg);
		$this->assertTrue($extended->extensions->xslt->enabled, $msg);
	}

	/**
	 * Tests extend() with mixing associative and numeric elements.
	 * @expectedException \Kojin\Config\ConfigException
	 */
	public function testExtendMixNumericAssociative() {
		$num = new Config(array("foo"));
		$assoc = new Config(array("foo" => "bar"));
		$num->extend($assoc);
	}

	/**
	 * Tests extend() with empty configs.
	 */
	public function testExtendEmpty() {
		$empty = new Config();
		$this->assertEquals($this->data, $this->config->extend($empty)->toArray());
		$this->assertEquals($this->data, $empty->extend($this->config)->toArray());
	}

	/**
	 * Tests get().
	 */
	public function testGet() {
		$data = $this->data;
		$config = $this->config;
		$this->assertEquals($data["foo"], $config->get("foo"));
		$this->assertEquals($data["animals"], $config->get("animals")->toArray());
		$this->assertEquals($data["people"], $config->get("people")->toArray());
		$this->assertEquals($data["people"]["jack"], $config->get("people.jack"));
		$this->assertEquals($data["people"]["jill"], $config->get("people.jill"));

		$this->assertNull($config->get("bar"));
		$this->assertNull($config->get("people.robert"));

		$this->assertNull($config->get(null));
		$this->assertNull($config->get(array()));
		$this->assertNull($config->get(""));
		$this->assertNull($config->get("."));
		$this->assertNull($config->get(".."));
	}
}
