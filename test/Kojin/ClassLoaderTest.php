<?php

namespace Kojin;

/**
 * Test for ClassLoader.
 */
class ClassLoaderTest extends \PHPUnit_Framework_TestCase {
	/**
	 * Tests getClassPath() with single namespace.
	 */
	public function testGetClassPath() {
		$loader = new ClassLoader("Space", "root");
		$this->assertEquals("root" . DS . "SomeClass.php", $loader->getClassPath("Space\SomeClass"));
		$this->assertFalse($loader->getClassPath("NotThere\Foo"));
	}

	/**
	 * Tests getClassPath() with multiple namespaces.
	 */
	public function testGetClassPathMultiple() {
		$loader = new ClassLoader("Foo\Bar", "somewhere");
		$this->assertEquals("somewhere" . DS . "Class.php", $loader->getClassPath("Foo\Bar\Class"));
		$this->assertFalse($loader->getClassPath("Foo\Qwe\Class"));
		$this->assertFalse($loader->getClassPath("Qwe\Bar\Class"));
	}

	/**
	 * Tests load() with class.
	 */
	public function testLoad() {
		$class = "Kojin\ClassLoaderTestClass";
		$loader = new ClassLoader("Kojin", dirname(__FILE__));
		$this->assertFalse(class_exists($class, false));
		$this->assertTrue($loader->load($class));
		$this->assertTrue(class_exists($class));

		// Load non-existant class
		$this->assertFalse($loader->load("\Kojin\ThisClassWillNeverExist"));
		// Load incorrect namesapce
		$this->assertFalse($loader->load("\UnknownNamespace\SomeClass"));
	}
	
	/**
	 * Tests load() for interface. 
	 */
	public function testLoadInterface() {
		$interface = "Kojin\ClassLoaderTestInterface";
		$loader = new ClassLoader("Kojin", dirname(__FILE__));
		$this->assertFalse(interface_exists($interface, false));
		$this->assertTrue($loader->load($interface));
		$this->assertTrue(interface_exists($interface));
	}
	
	/**
	 * Tests load() for trait. 
	 */
	public function testLoadTrait() {
		$trait = "Kojin\ClassLoaderTestTrait";
		$loader = new ClassLoader("Kojin", dirname(__FILE__));
		$this->assertFalse(trait_exists($trait, false));
		$this->assertTrue($loader->load($trait));
		$this->assertTrue(trait_exists($trait));
	}

	/**
	 * Tests getNamespace().
	 */
	public function testGetNamespace() {
		$loader = new ClassLoader("Kojin", dirname(__FILE__));
		$this->assertEquals("Kojin", $loader->getNamespace());

		$loader = new ClassLoader("Kojin\Config", dirname(__FILE__));
		$this->assertEquals("Kojin\Config", $loader->getNamespace());
	}
}
