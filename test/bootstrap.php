<?php

// Utility
defined("DS") || define("DS", DIRECTORY_SEPARATOR);

// Bootstrap Kojin
require_once(dirname(__FILE__) . DS . ".." . DS . "bootstrap.php");

// Define test constants - what are these used for?
// define("TEST_ROOT", dirname(__FILE__));
// define("DIR_ROOT", realpath(TEST_ROOT . DS . ".."));
define("SITE_ROOT", dirname(__FILE__) . DS . "site");
