<?php

namespace KojinExt\Image;

use Kojin\Kojin;
use Kojin\Fs\Directory;
use Kojin\Fs\File;
use Kojin\Content\Site;

/**
 * Tests for the ImageHandler.
 */
class ImageExtensionTest extends \PHPUnit_Framework_TestCase {
	public $site;
	public $dir;
	public $image;
	public $handler;
	
	public function setUp() {
		$kojin = new Kojin(SITE_ROOT);
		// Handler
		$this->handler = $kojin->getExtensionLoader()->loadExtension("image");
		// Site
		$this->site = $kojin->getSite();
		$this->site->setRootUrl("/site");
		// Images dir
		$this->dir = new Directory(SITE_ROOT.DS."content".DS."image");
		// Load simple test image
		$this->image = new Image($this->dir->getFile("hcandersen.jpg"));
	}

	/**
	 * Tests parseFiltersString().
	 */
	public function testParseFiltersString() {
		$tests = array(
			// Simple
			array("filter1", array(
				array("filter" => "filter1")
			)),
			// Double
			array("filter1; filter2", array(
				array("filter" => "filter1"),
				array("filter" => "filter2")
			)),
			// Simple var
			array("lighten(10)", array(
				array("filter" => "lighten", "arguments" => array(10))
			)),
			// Double var
			array("lighten(10, 20)", array(
				array("filter" => "lighten", "arguments" => array(10, 20))
			)),
			// Named argument
			array("scale(width=100, height=200)", array(
				array("filter" => "scale", "arguments" => array(
					"width" => 100,
					"height" => 200
				))
			)),
			// Percentages
			array("scale(70%)", array(
				array("filter" => "scale", "arguments" => array(0.7))
			)),
			// Complex
			array("desaturate(); scale(max=100); crop(100,100,center)", array(
				array("filter" => "desaturate"),
				array("filter" => "scale", "arguments" => array(
					"max" => 100
				)),
				array("filter" => "crop", "arguments" => array(100,100,"center"))
			)),
		);

		foreach ($tests as $test) {
			$msg = print_r($test, true);
			$supplied = $this->handler->parseFiltersString($test[0]);
			$expected = $test[1];
			$this->assertEquals($expected, $supplied, $msg);
		}
	}
	
	/**
	 * Tests loadImage().
	 */
	public function testLoadImage() {
		// Not inside root
		$this->assertNull(
			$this->handler->getImageFile($this->site, "/image.png")
		);
		// Inside root, but doesn't exist
		$this->assertNull($this->handler->loadImage(
			$this->handler->getImageFile($this->site, $this->site->getRootUrl()."/doesntexist.png")
		));
		// Inside root, and exists
		$image = $this->handler->loadImage(
			$this->handler->getImageFile($this->site, $this->site->getRootUrl()."/image/hcandersen.jpg")
		);
		$this->assertInstanceOf("\KojinExt\Image\Image", $image);
		$this->assertTrue($this->image->getFile()->exists());
		$this->assertEquals($this->image->getSignature(), $image->getSignature());
	}
	
	/**
	 * Tests filterImage() with scale.
	 */
	public function testFilterScale() {
		$filter = array(
			"filter" => "scale",
			"arguments" => array(
				"width" => 32,
				"height" => 32
			)
		);
		$this->handler->filterImage($this->image, array($filter));
		$this->assertEquals(32, $this->image->getWidth());
		$this->assertEquals(32, $this->image->getHeight());
		$this->assertEquals("d400f1e3cd517807b9b75434edde61b4de544ba509acd2d6f68c1a46ea61e1b9", $this->image->getSignature());
	}

	/**
	 * Tests that the original image is not loaded into memory when the cached version will do.
	 * @todo when a cached version is loaded, loadIamge() is also called. In order to unit test this properly, loadImage() should NEVER be called on cached versions.
	 */
	// public function testNoOriginalLoadOnCache() {
	// 	var_dump(__METHOD__);
	// 	// Load page and file contents.
	// 	// This html page contains a few images.
	// 	$page = $this->site->getPage("image/index");
	// 	$page->setFolder($this->site->getFolder("image"));
	// 	$html = $this->site->getContentDir()->getFile("image/index.html")->getContents();
	// 	// We need to rewrite the filenames slightly before passing them to the processor
	// 	$html = str_replace("hcandersen.jpg", $this->site->getRootUrl() . $page->getFolder()->getUrlPath() . "/hcandersen.jpg", $html);
	// 	// Process contents to make sure the images are cached
	// 	$processed = $this->handler->processPage($page, $html);
	// 	// Ensure contents were cached
	// 	$this->assertFalse(strpos($processed, "hcandersen.jpg"), $processed);
	// 	// Create mock object so we can count calls to loadImage()
	// 	$mock = $this->getMock(get_class($this->handler), array("loadImage"));
	// 	$mock->expects($this->never())
	// 		->method("loadImage")
	// 		->will($this->returnValue($this->image));
	// 	// Process page
	// 	echo "\n\n\n\n";
	// 	$mock->processPage($page, $html);
	// }
}
