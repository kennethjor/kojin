<?php

namespace KojinExt\Image\Filter;

use Kojin\Fs\Directory;
use Kojin\Fs\File;
use Kojin\Content\Site;
use KojinExt\Image\Image;

/**
 * Tests for the ImageHandler.
 */
class CropFilterTest extends \PHPUnit_Framework_TestCase {
	public $site;
	public $dir;
	public $image;
	public $filter;
	
	public function setUp() {
		// Handler
		$this->filter = new CropFilter();
		// Site
		$this->site = new Site(SITE_ROOT);
		$this->site->setRootUrl("/site");
		// Images dir
		$this->dir = new Directory(SITE_ROOT.DS."content".DS."image");
		// Load simple test image
		$this->image = new Image($this->dir->getFile("hcandersen.jpg"));
	}

	/**
	 * Tests calculateCropCoordinates().
	 */
	public function testCalculateCropCoordinates() {
		// Center center
		$imageSize = array(600, 600);
		$cropSize = array(400, 400);
		$position = array(0.5, 0.5);
		$expected = array(400, 400, 100, 100);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
		// Top left
		$position = array(0.0, 0.0);
		$expected = array(400, 400, 0, 0);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
		// Top right
		$position = array(1.0, 0.0);
		$expected = array(400, 400, 200, 0);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
		// Bottom right
		$position = array(1.0, 1.0);
		$expected = array(400, 400, 200, 200);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
		// Bottom left
		$position = array(0.0, 1.0);
		$expected = array(400, 400, 0, 200);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
		// Overflow
		$position = array(0.0, 0.0);
		$cropSize = array(700, 700);
		$expected = array(600, 600, 0, 0);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
		// More overflow
		$position = array(1.0, 1.0);
		$supplied = $this->filter->calculateCropCoordinates($imageSize, $cropSize, $position);
		$this->assertEquals($expected, $supplied);
	}
}
