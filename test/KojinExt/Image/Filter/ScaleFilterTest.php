<?php

namespace KojinExt\Image\Filter;

use Kojin\Fs\Directory;
use Kojin\Fs\File;
use Kojin\Content\Site;
use KojinExt\Image\Image;

/**
 * Tests for the ImageHandler.
 */
class ScaleFilterTest extends \PHPUnit_Framework_TestCase {
	public $site;
	public $dir;
	public $image;
	public $filter;
	
	public function setUp() {
		// Handler
		$this->filter = new ScaleFilter();
		// Site
		$this->site = new Site(SITE_ROOT);
		$this->site->setRootUrl("/site");
		// Images dir
		$this->dir = new Directory(SITE_ROOT.DS."content".DS."image");
		// Load simple test image
		$this->image = new Image($this->dir->getFile("hcandersen.jpg"));
	}

	/**
	 * Tests createAutoScaleFilters().
	 */
	public function testCreateAutoScaleFilters() {
		// Noop with no size
		$this->assertEquals(array(594, 800), $this->filter->calculateAspectResize($this->image, null, null));
		
		// Noop with same size
		$this->assertEquals(array(594, 800), $this->filter->calculateAspectResize($this->image, 594, 800));
		
		// No auto
		$this->assertEquals(array(32, 32), $this->filter->calculateAspectResize($this->image, 32, 32));
		
		// Auto height
		$this->assertEquals(array(148, 199), $this->filter->calculateAspectResize($this->image, 148, null));
		
		// Auto width
		$this->assertEquals(array(150, 202), $this->filter->calculateAspectResize($this->image, null, 202));
	}
}
