<?php

namespace KojinExt\Image;

use Kojin\Fs\Directory;
use Kojin\Fs\File;

/**
 * Tests for the Image.
 */
class ImageTest extends \PHPUnit_Framework_TestCase {
	public $dir;
	public $image;
	public $signature = "836789b8b2c974a90a13e9ea12d43f1476ef951f69ebdff0a005f34530bc7cb2";
	
	public function setUp() {
		$this->dir = new Directory(SITE_ROOT.DS."content".DS."image");
		// Load simple test image
		$this->image = new Image($this->dir->getFile("hcandersen.jpg"));
	}
	
	/**
	 * Tests getImagick().
	 * This primarily tests whether the image has loaded correctly.
	 */
	public function testGetImagick() {
		$imagick = $this->image->getImagick();
		$this->assertEquals(594, $imagick->getImageWidth());
		$this->assertEquals(800, $imagick->getImageHeight());
		$this->assertEquals("JPEG", $imagick->getImageFormat());
		$this->assertEquals($this->signature, $imagick->getImageSignature());
	}
	
	/**
	 * Tests getSignature
	 */
	public function testGetSignature() {
		$this->AssertEquals($this->signature, $this->image->getSignature());
	}
	
	/**
	 * Tests scale.
	 */
	public function testScale() {
		$this->image->scale(32, 32);
		$this->assertEquals(32, $this->image->getWidth());
		$this->assertEquals(32, $this->image->getHeight());
		$this->assertEquals("d400f1e3cd517807b9b75434edde61b4de544ba509acd2d6f68c1a46ea61e1b9", $this->image->getSignature());
	}
	
	/**
	 * Tests save().
	 */
	public function testSave() {
		$dir = Directory::createTempDir("Kojin-ImageTest-" . __LINE__);
		$file = $dir->getFile("test.png");
		$this->image->save($file);
		$this->assertTrue($file->exists());
		$image = new Image($file);
		$this->assertEquals($this->signature, $image->getSignature());
		$dir->removeRecursive();
	}
}
