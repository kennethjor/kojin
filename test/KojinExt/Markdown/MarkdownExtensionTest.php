<?php

namespace KojinExt\Markdown;

use Kojin\Kojin;
use Kojin\Fs\File;

/**
 * Tests Markdown.
 */
class MarkdownExtensionTest extends \PHPUnit_Framework_TestCase {
	/** @var \KojinExt\Markdown\Markdown */
	public $markdown;

	public $testMarkdown;
	public $testHtml;

	public function setUp() {
		MarkdownExtension::onLoad();
		$this->markdown = new MarkdownExtension();
		$this->testMarkdown = file_get_contents(dirname(__FILE__) . DS . "test.md");
		$this->testHtml = file_get_contents(dirname(__FILE__) . DS . "test.html");
	}

	/**
	 * Tests loadSource().
	 */
	public function testLoadSource() {
		$supplied = $this->markdown->loadSource($this->testMarkdown);
		$this->assertEquals($this->testHtml, $supplied);
	}

	/**
	 * Tests loadFile().
	 */
	public function testLoadFile() {
		$supplied = $this->markdown->loadFile(new File(dirname(__FILE__) . DS . "test.md"));
		$this->assertEquals($this->testHtml, $supplied);
	}
}
