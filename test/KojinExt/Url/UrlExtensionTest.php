<?php

namespace KojinExt\Url;

use Kojin\Kojin;
use Kojin\Contect\Site;

/**
 * Tests for the Url extension.
 */
class UrlExtensionTest extends \PHPUnit_Framework_TestCase {
	public $site;
	public $page;

	public $url;

	public function setUp() {
		$kojin = new Kojin(SITE_ROOT);
		$this->site = $kojin->getSite();
		$this->page = $this->site->getPage("/foo/bar");
		$this->url = $kojin->getExtensionLoader()->loadExtension("url");
	}

	/**
	 * Tests processUrl() with URLs, which should not be handled at all.
	 */
	public function testProcessUrlAbsolute() {
		$urls = array(
			"",
			"/foo",
			"/bar",
			"/foo/bar",
			"http://example.org",
			"https://example.org",
			"http://example.org/foo/bar",
			"https://example.org/foo/bar",
			"protocol://user:password@who/knows",
			"[something]/foo/bar"
		);
		foreach ($urls as $i => $url) {
			$msg = "i:$i :: $url";
			$this->assertEquals($url, $this->url->processUrl($this->page, $url), $msg);
		}
	}

	/**
	 * Tests processUrl() with site relative URLs.
	 */
	public function testProcessUrlSiteRelative() {
		$root = "/somewhere/below";
		$this->site->setRootUrl($root);
		$urls = array(
			array("$root", "//"),
			array("$root/foo", "//foo"),
			array("$root/foo/bar", "//foo/bar"),
			array("$root/foo/bar/other", "//foo/bar///other")
		);
		foreach ($urls as $i => $url) {
			$msg = "i:$i :: {$url[0]} :: {$url[1]}";
			$this->assertEquals($url[0], $this->url->processUrl($this->page, $url[1]), $msg);
		}
	}

	/**
	 * Tests processUrl() with page relative URLs.
	 */
	public function testProcessUrlPageRelative() {
		$siteRoot = "/somewhere/below";
		$urlPath = "/foo/bar";
		$this->site->setRootUrl($siteRoot);
		$this->page->setUrlPath($urlPath);

		$dir = "/somewhere/below/foo";
		
		$urls = array(
			array("$dir/page", "page"),
			array("$dir/qwe/asd", "qwe/asd"),
			array("$dir/qwe/asd", "qwe/////asd")
		);
		foreach ($urls as $i => $url) {
			$msg = "i:$i :: {$url[0]} :: {$url[1]}";
			$this->assertEquals($url[0], $this->url->processUrl($this->page, $url[1]), $msg);
		}
	}

	/**
	 * Tests the regex.
	 */
	public function testRegex() {
		$regex = $this->url->getRegex();
		
		$tests = array(
			array("<a href=\"somewhere\">",      "somewhere"),
			array("<a href='somewhere'>",        "somewhere"),
			array("<a href=\"somewhere\" />",    "somewhere"),
			array("<a href='somewhere' />",      "somewhere"),

			array("<link href=\"somewhere\">",   "somewhere"),
			array("<link href='somewhere'>",     "somewhere"),
			array("<link href=\"somewhere\" />",   "somewhere"),
			array("<link href='somewhere' />",     "somewhere"),

			array("<script href=\"somewhere\">", "somewhere"),
			array("<script href='somewhere'>",   "somewhere"),
			array("<script href=\"somewhere\"/>", "somewhere"),
			array("<script href='somewhere'/>",   "somewhere"),

			array("<something href=\"somewhere\">"),
			array("<something href='somewhere'>")
		);
		$regex = $this->url->getRegex();
//		echo "REGEX: $regex\n";
		$n = 0;
		foreach ($tests as $i => $test) {
			$input = $test[0];
			$msg = "i: $i\nregex: $regex\ninput: $input\n";
			// With match
			if (array_key_exists(1, $test)) {
				$c = preg_match($regex, $input, $matches);
				$msg .= "c: $c\nmatches: " . print_r($matches, true) . "\n";
				$this->assertArrayHasKey("url", $matches, $msg);
				$this->assertEquals($test[1], $matches["url"], $msg);
			}
			// Without match
			else {
				$this->assertEquals(0, preg_match($regex, $input), $msg);
			}

			$n++;
		}
		$this->assertEquals(count($tests), $n);
	}

	/**
	 * Tests process().
	 */
	public function testProcess() {
		$siteRoot = "/somewhere/below";
		$this->site->setRootUrl($siteRoot);
		$tests = array(
			// Anchor single quote
			array(
				"<a href='//htmlpage'>HTML page</a>",
				"<a href='$siteRoot/htmlpage'>HTML page</a>"
			),
			// Anchor double quote
			array(
				"<a href=\"//htmlpage\">HTML page</a>",
				"<a href=\"$siteRoot/htmlpage\">HTML page</a>"
			),
			// Link single quote
			array(
				"<link rel='stylesheet' type='text/css' href='//styles.css'>",
				"<link rel='stylesheet' type='text/css' href='$siteRoot/styles.css'>"
			),
			// Link double quote
			array(
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"//styles.css\">",
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"$siteRoot/styles.css\">"
			),
			// Script single quote
			array(
				"<script type='text/javascript' src='//script.js'></script>",
				"<script type='text/javascript' src='$siteRoot/script.js'></script>"
			),
			// Script double quote
			array(
				"<script type=\"text/javascript\" src=\"//script.js\"></script>",
				"<script type=\"text/javascript\" src=\"$siteRoot/script.js\"></script>"
			)
		);
		foreach ($tests as $test) {
			$msg = "input: {$test[0]}";
			$this->assertEquals($test[1], $this->url->processPage($this->page, $test[0]), $msg);
		}
	}
}
