<?php

namespace KojinExt\VarSub;

use Kojin\Kojin;
use Kojin\Contect\Site;

/**
 * Tests for VarSubProcessor.
 */
class VarsubExtensionTest extends \PHPUnit_Framework_TestCase {
	public $kojin;
	public $site;
	public $page;

	public $varsub;

	public function setUp() {
		$this->kojin = new Kojin(SITE_ROOT);
		$this->site = $this->kojin->getSite();
		$this->page = $this->site->getPage("/varsub/test");
		$this->varsub = $this->kojin->getExtensionLoader()->loadExtension("varsub");
	}

	/**
	 * Tests getRegex().
	 */
	public function testGetRegex() {
		$tests = array(
			array("[kojin:site:title]", array(
				"namespace" => "site",
				"var" => "title"
			)),
			array("[nothing][kojin:site:rootUrl] something", array(
				"namespace" => "site",
				"var" => "rootUrl"
			)),
			array("kojin:something:else [kojin:site:rootUrl]", array(
				"namespace" => "site",
				"var" => "rootUrl"
			)),
			array("kojin:something:else [kojin:qwerty:foo.bar]", array(
				"namespace" => "qwerty",
				"var" => "foo.bar"
			))
		);
		$regex = $this->varsub->getRegex();
		$n = 0;
		foreach ($tests as $test) {
			$tag = $test[0];
			$expected = $test[1];
			$msg = "tag: $tag\nregex: $regex\nexpected: " . print_r($expected, true);
			// Parse
			preg_match($regex, $tag, $matches);
			$msg .= " \nmathces: " . print_r($matches, true);
			// Assert
			foreach ($expected as $m => $v) {
				$this->assertArrayHasKey($m, $matches, $msg);
				$this->assertEquals($v, $matches[$m], $msg);
			}

			$n++;
		}
		$this->assertEquals(count($tests), $n);
	}

	/**
	 * Tests process().
	 */
	public function testProcess() {
		$site = $this->site;
		$page = $this->page;
		$varsub = $this->varsub;
		$siteConf = $site->getConfig();
		$pageConf = $page->getConfig();
		// Special variables
		$this->assertEquals($site->getRootUrl(), $varsub->processPage($page, "[kojin:site:rootUrl]"));
		// Config variables
		$this->assertEquals($siteConf->title, $varsub->processPage($page, "[kojin:site:title]"));
		$this->assertEquals($siteConf->layout, $varsub->processPage($page, "[kojin:site:layout]"));
		$this->assertEquals($pageConf->title, $varsub->processPage($page, "[kojin:page:title]"));
		$this->assertEquals($pageConf->date, $varsub->processPage($page, "[kojin:page:date]"));
		$this->assertEquals($pageConf->get("qwe.rty"), $varsub->processPage($page, "[kojin:page:qwe.rty]"));
		// Unknown variables
		$this->assertEquals("[kojin:site:unknown]", $varsub->processPage($page, "[kojin:site:unknown]"));
		$this->assertEquals("[kojin:page:unknown]", $varsub->processPage($page, "[kojin:page:unknown]"));
		$this->assertEquals("[kojin:something:else]", $varsub->processPage($page, "[kojin:something:else]"));
		// Invalid variable names
		$this->assertEquals("[kojin:something.else:foo.bar]", $varsub->processPage($page, "[kojin:something.else:foo.bar]"));
	}
}
