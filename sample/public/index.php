<?php

/**
 * This is the main index file.
 * It's responsible for loading and running Kojin.
 * It shouldn't contain anything else than that.
 * Modify the two constants below to suit your site.
 */

define("DS", DIRECTORY_SEPARATOR);

// Bootstrap Kojin
require_once(dirname(__FILE__) . DS . ".." . DS . ".." . DS . "bootstrap.php");

// Define constants - modify this to suit your directory structure
define("SITE_ROOT", realpath(dirname(__FILE__) . DS . ".."));

// Init and run Kojin
$kojin = new Kojin\Kojin(SITE_ROOT);
$kojin->run();
