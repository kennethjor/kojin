Crop filter
===========

<img src="kiyomizu-dera.jpg" width="400" />

    crop(400, 400, left, top)

<img src="kiyomizu-dera.jpg" filters="crop(400, 400, left, top)" />

    crop(400, 400, center, middle)

<img src="kiyomizu-dera.jpg" filters="crop(400, 400, center, middle)" />

    crop(400, 400, right, bottom)

<img src="kiyomizu-dera.jpg" filters="crop(400, 400, right, bottom)" />
