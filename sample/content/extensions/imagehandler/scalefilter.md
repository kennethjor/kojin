Scale filter
============
The scale filter provides simple image resizing much like is done when defining the `width` and `height` attributes.
In fact, the scale filter is used for this operation in the background.

Simple resizing
---------------
A simple resize is done as follow, with the two commands below being identical:

    scale(width=500, height=200)
    scale(500, 200)

<img src="kiyomizu-dera.jpg" filters="scale(width=500, height=200)" />
<img src="kiyomizu-dera.jpg" filters="scale(500, 200)" />

These will both scale the image to a size of 500x200.

As when using the `width` and `height` attributes during simple resizing, omitting either width or height will cause the image handler to maintain aspect ratio.

Min/max resizing
----------------
To scale an image based on the long side, use the `max` argument.
This will cause a 1000x2000 image to be scaled to 250x500.

    scale(max=300)

<img src="kiyomizu-dera.jpg" filters="scale(max=300)" />

Similarly a `min` argument can be supplied to scale based on the short side.

    scale(min=300)

<img src="kiyomizu-dera.jpg" filters="scale(min=300)" />

This would cause a 1000x2000 image to be scaled to 500x1000.

Relative resizing
-----------------
Lastly, a simple percentage can be supplied to just scale the image outright.

    scale(20%)
    scale(15%)
    scale(10%)

<img src="kiyomizu-dera.jpg" filters="scale(20%)" />
<img src="kiyomizu-dera.jpg" filters="scale(15%)" />
<img src="kiyomizu-dera.jpg" filters="scale(10%)" />