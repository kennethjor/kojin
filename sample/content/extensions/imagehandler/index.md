The ImageHandler extension
==========================
The image handler provides a mechanism for filtering images before being displayed to the client.
This means that images can be stored in their original form, allowing directives in the content to decide how they should be displayed.

All examples below are done using [kiyomizu-dera.jpg](kiyomizu-dera.jpg).

Simple resizing
---------------
To display a resized image, simply include the `width` and `height` attributes on your image tag.

    <img src="kiyomizu-dera.jpg" width="200" height="100" />

<img src="kiyomizu-dera.jpg" width="200" height="100" />

This of course will stretch the image to produce exactly the desired size.
By including only one of the size attributes, the handler will automatically calculate the other to preserve aspect ratio.

    <img src="kiyomizu-dera.jpg" width="200" />
    <img src="kiyomizu-dera.jpg" height="150" />

<img src="kiyomizu-dera.jpg" width="200" />
<img src="kiyomizu-dera.jpg" height="150" />

Filters
-------
In addition to adding width and height attributes to images, Kojin has a more powerful on-demand image manipulation tool.
These extra filters are controlled by defining the `filters` attribute on an image.
Each filter in the list is separated by a semi-colon, and arguments are supplied via brackets.
Both named and unnamed arguments are supported, with more details supplied for individual filters.

As an example, the following filter will scale an image to at least 500 pixels on either side.
Then it will square crop it to 400x400 px, essentially cutting off 20% of the edge.
Finally, it will desaturate it, making it black and white.

    <img src="kiyomizu-dera.jpg" filters="scale(max=500); crop(400, 400, center); desaturate" />

Currently the following filters are included in Kojin by default:

* [Scale filter](scalefilter)
* [Crop filter](cropfilter)
* [Grayscale filter](grayscalefilter)
