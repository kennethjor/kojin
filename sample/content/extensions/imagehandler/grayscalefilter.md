Grayscale filter
================
The grayscale filter can be activated by using either one of two filter strings:
    grayscale
or
    greyscale
The filter accepts no arguments

<img src="kiyomizu-dera.jpg" filters="scale(max=500); grayscale" />
