Extensions
==========
All content rendering, url handling, image resizing, etc. which Kojin supports out of the box is all implemented using extensions.
Kojin has a simple way of writing extensions for it, allowing you to modify content in any way you see fit.

Bundled extensions
------------------
Kojin comes with a number of bundled extensions which sites can make use of.

* The Html extension handles displaying of content written in HTML. This is probably the simplest content handler in the project.
* The [ImageHandler](imagehandler) extension is responsible for automatic image filtering and caching.
* The Markdown extension handles rendering of text files written in Markdown.
* The Url extension handles rewriting of URLs present in the content.
* The VarSub extension provides a very simple variable substitution engine.
