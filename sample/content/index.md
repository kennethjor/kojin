Kojin is a simple PHP framework designed for easy implementation of smaller websites.
It was designed as an alternative to other popular CMSs and is intented for people who are much more comfortable with text editors than fancy GUIs.

* [Extensions](extensions)
* [Source code][kojin source]

License
=======
Kojin is licensed the MIT License.
For further details see the `LICENSE` file in the root directory.
Kojin also comes with a few bundled libraries for which the licenses may differ.


[kojin source]: https://bitbucket.org/kennethjor/kojin "Kojin source"
