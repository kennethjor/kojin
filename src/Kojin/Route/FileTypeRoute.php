<?php

namespace Kojin\Route;

/**
 * A route which automatically matches against file types on the file system.
 */
class FileTypeRoute extends AbstractRoute {
	/**
	 * Filetypes matched by the route, and their associated mimetypes if defined.
	 * @var array.
	 */
	private $types = array();

	/**
	 * Adds a filetype to the route.
	 * @param $name string
	 * @return Kojin\Route\Route
	 */
	public function addFileType($ext, $mimetype = null) {
		$this->types[$ext] = $mimetype;
		return $this;
	}

	/**
	 * Returns all the filetypes associated with the route.
	 * @return array
	 */
	public function getFileTypes() {
		return $this->types;
	}

	public function execute($path) {
		// Import valid extensions
		$types = $this->getFileTypes();
		// Create relative file path
		$filePath = str_replace("/", DS, $path);
		// Fetch root path
		$root = $this->getKojin()->getSite()->getContentDir();

		// Check for direct file access
		$file = $root->getFile($filePath);
		if ($file->exists()) {
			$ext = $file->getExtension();
			// Validate type
			if (array_key_exists($ext, $types)) {
				return array(
					"mimetype" => $types[$ext],
					"file" => $file,
					"path" => $path
				);
			}
		}

		// No direct access, iterate over valid types
		foreach ($types as $ext => $mimetype) {
			$file = $root->getFile($filePath.".".$ext);
			if ($file->exists()) {
				$ext = $file->getExtension();
				// Validate type
				if (array_key_exists($ext, $types)) {
					return array(
						"mimetype" => $types[$ext],
						"file" => $file,
						"path" => $path.".".$ext
					);
				}
			}
		}

		// Check folder access
		$folder = $root->getDir($filePath);
		if ($folder->exists()) {
			if (substr($path, -1) !== "/") {
				$path .= "/";
			}
			$path .= "index";
			return $this->execute($path);
		}

		// Nothing
		return false;
	}
}
