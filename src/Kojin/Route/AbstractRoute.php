<?php

namespace Kojin\Route;

use Kojin\HasKojin;

/**
 * A route.
 */
abstract class AbstractRoute {
	use HasKojin;

	/**
	 * The name of the route.
	 * @var string.
	*/
	private $name;

	/**
	 * Sets the name of the route.
	 * @param $name string
	 * @return Kojin\Route\Route
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * Returns the name of the route.
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Executes the route against a supplied path.
	 * If this route matches, this function will return an associative array
	 * containing information delivered by the route.
	 * If this route does not match the supplied path, fale is returned.
	 * The rotue itself MUST NOT contruct or prepare any targets.
	 * @param $path string
	 * @return array|boolean
	 */
	public abstract function execute($path);
}
