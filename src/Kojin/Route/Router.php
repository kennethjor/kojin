<?php

namespace Kojin\Route;

use Kojin\HasKojin;
use Kojin\Content\NotFoundException;
use Kojin\Fs\File;

/**
 * The Kojin router.
 */
class Router {
	use HasKojin;
	
	/**
	 * The root URL.
	 * @var string
	 */
	private $rootUrl;

	/**
	 * Registered routes.
	 * @var array
	 */
	private $routes = array();
	
	/**
	 * Constructor
	 * @param string $root the root URL of this router.
	 */
	public function __construct($root = null) {
		if (is_string($root)) {
			$this->rootUrl = self::sanitizeUrl($root);
		}
	}

	/**
	 * Sets the root URL.
	 * @param string $rootUrl
	 */
	public function setRootUrl($rootUrl) {
		$this->rootUrl = $rootUrl;
	}

	/**
	 * Returns the root URL.
	 * @return string
	 */
	public function getRootUrl() {
		return $this->rootUrl;
	}

	/**
	 * Returns the relative path of URL based on the root.
	 * @param string $root
	 * @param string $url
	 * @return string
	 */
	public function detectRelativePath($root, $url) {
		// Sanitize
		$root = self::sanitizeUrl($root);
		$url = self::sanitizeUrl($url);
		// Make sure URL is inside root
		if (substr($url, 0, strlen($root)) !== $root) {
			throw new \Exception("URL ($url) is not inside detected base ($root)");
		}
		// Prepare path by removing the base from the URL
		$path = substr($url, strlen($root));
		// Detect root
		if (strlen($path) === 0) {
			$path = "/";
		}
		// Execute site
		return $path;
	}

	/**
	 * Returns the relative path from the root URL configured on this router.
	 * If URL is not supplied, the router will try and detect it from the environment.
	 * @param string $url
	 * @return string
	 */
	public function getRelativePath($url = null) {
		if (is_null($url)) {
			$url = $_SERVER["REQUEST_URI"];
		}
		if (is_null($this->rootUrl)) {
			throw new \Exception("No root URL configured.");
		}
		return $this->detectRelativePath($this->rootUrl, $url);
	}

	/**
	 * Creates a router with the root path configured from the PHP environment.
	 * @return string
	 */
	public static function createRouter() {
		$root = $_SERVER["PHP_SELF"];
		// Remove index.php
		if (basename($root) === "index.php") {
			$root = dirname($root);
		}
		return new static($root);
	}

	/**
	 * Sanitizes a URL.
	 * @param string $url
	 * @return string
	 */
	public static function sanitizeUrl($url) {
		// Replace illegal characters
		$url = preg_replace("/[^a-zA-Z0-9%~\.\/_+-\[\]!@]/", "", $url);
		// Replace double slashes
		$url = preg_replace("/\/{2,}/", "/", $url);
		// Run through file's getAbsolutePath to handle relative element
		$file = new File(str_replace("/", DS , $url));
		$url = str_replace(DS, "/", $file->getAbsolutePath());
		// $file will preserve initial relative parts, we don't want them
		$url = preg_replace("/\/?(\.\.\/?)+/", "", $url);
		// Detect root
		if (strlen($url) == 0) {
			$url = "/";
		}
		
		// Remove trailing slash
		if (strlen($url) > 1 && substr($url, -1) === "/") {
			$url = substr($url, 0, strlen($url)-1);;
		}
		
		// Ensure leading slash
		if (strlen($url) > 1 && substr($url, 0, 1) !== "/") {
			$url = "/" . $url;
		}

		return $url;
	}
	
	/**
	 * Routes a path and returns the detected content object.
	 * @param $path string|null
	 * @return \Kojin\Content\AbstractContent
	 */
	public function route($path = null) {
		$kojin = $this->getKojin();
		$site = $kojin->getSite();
		// Automatic path resolution
		if (is_null($path)) {
			$path = $this->getRelativePath();
		}
		// Check site
		if (is_null($site)) {
			throw new KojinException("Site not set for routing.");
		}
		// Iterate over available routes
		foreach ($this->routes as $route) {
			// Execute route
			$result = $route["route"]->execute($path);
			if (is_array($result) === false) {
				continue;
			}
			// We have a matching route, fetch the page loader from the list
			$extensions = $kojin->getExtensionLoader()->loadExtensions();
			$extension = $extensions[$route["handler"]];
			// Prepare page
			$page = $site->getPage($result["path"]);
			$page->setFile($result["file"]);
			$page->setPageLoader($extension);

			return $page;
		}
		
		// Nothing was found
		throw new NotFoundException($path);
	}

	public function addRoute($name, AbstractRoute $route, $handler) {
		$route->setKojin($this->getKojin());
		$this->routes[$name] = array(
			"route" => $route,
			"handler" => $handler
		);
		return $this;
	}

	/**
	 * Returns all the currently configured rotues.
	 * @return array of Route objects with name as key.
	 */
	public function getRoutes() {
		return $this->routes;
	}
}
