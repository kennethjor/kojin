<?php

namespace Kojin;

/**
 * Kojin's class loader.
 */
class ClassLoader {
	/**
	 * The namespace controlled by this autoloader.
	 * @var string
	 */
	private $namespace;

	/**
	 * The root directory to load from.
	 * @var string
	 */
	private $root;

	/**
	 * Cosntructor.
	 * @param string $namespace
	 * @param string $root
	 */
	public function __construct($namespace, $root) {
		if (is_string($namespace) === false) {
			throw new \Exception("Namespace must be a string");
		}
		if (is_string($root) === false) {
			throw new \Exception("Root must be a string");
		}
		$this->namespace = explode("\\", $namespace);
		$this->root = $root;
	}

	/**
	 * Creates expected path for class file.
	 * @param string $class
	 * @return string
	 */
	public function getClassPath($class) {
		// Split
		$spaces = explode("\\", $class);
		// Verify namespace
		foreach ($this->namespace as $s) {
			if (array_shift($spaces) !== $s) {
				return false;
			}
		}
		// Generate path
		$path = $this->root . DS . implode(DS, $spaces) . ".php";
		
		return $path;
	}

	/**
	 * Loads a class.
	 * @param string $class
	 */
	public function load($class) {
		$path = $this->getClassPath($class);
		if (is_file($path) === false) {
			return false;
		}
		// Load
		require_once($path);
		// Verify
		return class_exists($class, false)
			|| interface_exists($class, false)
			|| trait_exists($class, false);
	}

	/**
	 * Returns the namespace.
	 * @return string
	 */
	public function getNamespace() {
		return implode("\\", $this->namespace);
	}
}
