<?php

namespace Kojin\Fs;

/**
 * Base class for file system resources. 
 */
abstract class FsResource {
	/** @var \SplFileInfo */
	protected $fileInfo;
	
	/**
	 * Constructor.
	 * @param string $path 
	 */
	public function __construct($path) {
		// Sanitize
		$path = str_replace(DS.DS, DS, $path);
		while (strlen($path) > 1 && substr($path, -1) === DS) {
			$path = substr($path, 0, -1);
		}
		
		$this->fileInfo = new \SplFileInfo($path);
	}
	
	/**
	 * Retruns true if the resource exists.
	 * @return bool
	 */
	public abstract function exists();
	
	/**
	 * Returns the path to the file.
	 * @return string 
	 */
	public function getPath() {
		return $this->fileInfo->getPathname();
	}
	
	/**
	 * Returns the real path of the file, or false if the file doesn't exist. 
	 */
	public function getRealPath() {
		return $this->fileInfo->getRealPath();
	}
	
	/**
	 * Returns the basename of the resource.
	 * @return string 
	 */
	public function getBaseName() {
		return $this->fileInfo->getBaseName();
	}
	
	/**
	 * Returns the absolute path.
	 * This is different from realpath in that we don't check for existance or follow symlinks. 
	 */
	public function getAbsolutePath() {
		$path = explode(DS, $this->getPath());
		$firstRel = -1;
		for ($i=0 ; $i<count($path) ; $i++) {
			if ($path[$i] == "..") {
				if ($i == $firstRel+1) {
					$firstRel++;
				}
				else {
					array_splice($path, $i-1, 2);
					$i -= 2;
				}
			}
		}
		
		return implode(DS, $path);
	}
	
	/**
	 * Returns true if this resouce is readable.
	 * @return bool 
	 */
	public function isReadable() {
		return $this->fileInfo->isReadable();
	}
	
	/**
	 * Returns true if this resouce is readable.
	 * @return bool 
	 */
	public function isWritable() {
		return $this->fileInfo->isWritable();
	}
	
	/**
	 * Returns the permissions of the resource.
	 * @return int
	 */
	public function getPerms() {
		return $this->fileInfo->getPerms();
	}
}
