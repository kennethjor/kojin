<?php

namespace Kojin\Fs;

/**
 * Trait for allowing easy access to object with a file. 
 */
trait HasFile {
	/**
	 * The file.
	 * @var \Kojin\Fs\File 
	 */
	private $trait_hasFile_file;
	
	/**
	 * Sets the file.
	 * @param \Kojin\Fs\File|string $file 
	 */
	public function setFile($file) {
		if (is_string($file)) {
			$file = new File($file);
		}
		$this->trait_hasFile_file = $file;
	}
	
	/**
	 * Returns the file.
	 * @return \Kojin\Fs\File 
	 */
	public function getFile() {
		return $this->trait_hasFile_file;
	}
}
