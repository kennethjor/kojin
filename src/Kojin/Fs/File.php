<?php

namespace Kojin\Fs;

use Kojin\KojinException;

/**
 * Represents a physical file on the server. 
 */
class File extends FsResource {
	/**
	 * Returns true if the file exists.
	 * @return bool 
	 */
	public function exists() {
		return $this->fileInfo->isFile();
	}
	
	/**
	 * Returns the directory the file is in.
	 * @return \Kojin\Fs\Directory 
	 */
	public function getDir() {
		return new Directory($this->fileInfo->getPath());
	}
	
	/**
	 * Returns the extension of the file.
	 * @return string 
	 */
	public function getExtension() {
		return $this->fileInfo->getExtension();
	}
	
	/**
	 * Returns the contents of the file.
	 * @return string 
	 */
	public function getContents() {
		if ($this->exists() === false) {
			throw new FsException("File not found: " . $this->getPath());
		}
		if ($this->isReadable() === false) {
			throw new FsException("File not readable: " . $this->getPath());
		}
		return file_get_contents($this->getPath());
	}
	
	/**
	 * Commits contents to the file.
	 * @param string $contents
	 */
	public function putContents($contents) {
		if ($this->exists() === false) {
			$dir = $this->getDir();
			if ($dir->exists() === false) {
				throw new FsException("Directory not found: " . $dir->getPath());
			}
			if ($dir->isWritable() === false) {
				throw new FsException("Directory not writable: " . $dir->getPath());
			}
		}
		elseif ($this->isWritable() === false) {
			throw new FsException("File not writable: " . $this->getPath());
		}
		return file_put_contents($this->getPath(), $contents);
	}
	
	/**
	 * Touches the file.
	 */
	public function touch() {
		return touch($this->getPath());
	}
	
	/**
	 * Removes the file.
	 */
	public function remove() {
		return unlink($this->getPath());
	}

	/**
	 * Returns a SHA1 hash of the file contents, or null if the file was not found.
	 */
	public function getSha1Hash() {
		if ($this->exists()) {
			return sha1_file($this->getPath());
		}
		return null;
	}
}
