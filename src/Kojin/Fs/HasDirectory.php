<?php

namespace Kojin\Fs;

/**
 * Trait for allowing easy access to object with a directory. 
 */
trait HasDirectory {
	/**
	 * The directory.
	 * @var \Kojin\Fs\Directory 
	 */
	private $trait_hasDirectory_directory;
	
	/**
	 * Sets the directory.
	 * @param \Kojin\Fs\Directory|string $file 
	 */
	public function setDir($dir) {
		if (is_string($dir)) {
			$dir = new Directory($dir);
		}
		elseif ($dir instanceof Directory === false) {
			throw new FsException("Supplied dir must be either a string or a Directory object.");
		}
		$this->trait_hasDirectory_directory = $dir;
	}
	
	/**
	 * Returns the directory.
	 * @return \Kojin\Fs\Directory 
	 */
	public function getDir() {
		return $this->trait_hasDirectory_directory;
	}
}
