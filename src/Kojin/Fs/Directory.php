<?php

namespace Kojin\Fs;

/**
 * Represents a physical directory on the server. 
 */
class Directory extends FsResource {
	/**
	 * Returns true if the directory exists.
	 * @return bool 
	 */
	public function exists() {
		return $this->fileInfo->isDir();
	}
	
	/**
	 * Returns a file inside this directory. 
	 */
	public function getFile($path) {
		return new File($this->getPath().DS.$path);
	}
	
	/**
	 * Returns a directory inside this directory. 
	 */
	public function getDir($path) {
		return new Directory($this->getPath().DS.$path);
	}
	
	/**
	 * Creates the directory if it doesn't exist.
	 * This method always automatically creates subdirectories.
	 * @param int $mode
	 * @return bool true if the directory was created or already existed
	 */
	public function create($mode = 0777) {
		if ($this->exists()) {
			return true;
		}
		return mkdir($this->getPath(), $mode, true);
	}
	
	/**
	 * Removes the directory if it exists.
	 * @return bool true if the directory was removed
	 */
	public function remove() {
		if ($this->exists() === false) {
			throw new FsException("Directory not found: " . $this->getPath());
		}
		return rmdir($this->getPath());
	}
	
	/**
	 * Removes the directory recusively.
	 * @return bool
	 */
	public function removeRecursive() {
		// Prevent stupid mistakes
		if ($this->getPath() == "/") {
			throw new FsException("Refusing to remove root");
		}
		// Delete contents
		foreach ($this->getContents() as $c) {
			if ($c instanceof Directory) {
				if ($c->removeRecursive() === false) {
					return false;
				}
			}
			elseif ($c instanceof File) {
				if ($c->remove() === false) {
					return false;
				}
			}
		}
		// Delete self
		return $this->remove();
	}
	
	/**
	 * Returns the contents of the directory as an array.
	 * @return array
	 */
	public function getContents() {
		if ($this->exists() === false) {
			throw new FsException("Directory doesn't exist: " . $this->getPath());
		}
		$contents = array();
		$iterator = new \DirectoryIterator($this->getPath());
		foreach ($iterator as $i) {
			// Skip no-real elements
			$filename = $i->getFilename();
			if (strlen($filename) == 0 || $filename == "." || $filename == "..") {
				continue;
			}
			// Create objects
			if ($i->isFile()) {
				$contents[] = new File($i->getPathname());
			}
			elseif ($i->isDir()) {
				$contents[] = new Directory($i->getPathname());
			}
		}
		return $contents;
	}
	
	/**
	 * Shorthand method for creating directories.
	 * @param string $path
	 * @return \Kojin\Fs\Directory
	 */
	public static function getCreate($path) {
		$dir = new static($path);
		$dir->create();
		return $dir;
	}
	
	/**
	 * Creates a temporary directory.
	 * @param string $prefix a prefix added to the directory
	 * @return \Kojin\Fs\Directory
	 */
	public static function createTempDir($prefix = null) {
		// Prepare base
		$base = sys_get_temp_dir() . DS;
		if (is_string($prefix)) {
			$base .= $prefix . "-";
		}
		// Create non-existant name
		$path = null;
		do {
			// Random
			$path = $base . substr(md5(rand()), 0, 10);
		} while ( (is_file($path) | is_dir($path) | is_link($path)) === false );
		// Create dir
		return static::getCreate($path);
	}
}
