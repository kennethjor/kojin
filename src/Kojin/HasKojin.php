<?php

namespace Kojin;

/**
 * Trait to easily attach Kojin acces to a class. 
 */
trait HasKojin {
	/**
	 * The Kojin instance.
	 * @var \Kojin\Kojin
	 */
	private $trait_hasKojin_kojin;
	
	/**
	 * Sets the Kojin instance.
	 * @param \Kojin\Kojin $kojin 
	 */
	public function setKojin(Kojin $kojin) {
		$this->trait_hasKojin_kojin = $kojin;
	}
	
	/**
	 * Returns the Kojin instance.
	 * @return \Kojin\Kojin
	 */
	public function getKojin() {
		return $this->trait_hasKojin_kojin;
	}
}

