<?php

namespace Kojin\Extension;

use Kojin\Kojin;
use Kojin\HasKojin;
use Kojin\KojinException;
use Kojin\ClassLoader;
use Kojin\Config\Config;
use Kojin\Content\HasSite;
use Kojin\Content\HasPage;
use Kojin\Fs\Directory;

/**
 * Loads and manages extensions.
 */
class ExtensionLoader {
	use HasSite;
	use HasPage;
	use HasKojin;
	
	/**
	 * Configured extension directories.
	 * @var array
	 */
	private $extensionDirs = array();

	/**
	 * The configured extension config.
	 * @var \Kojin\Config\Config
	 */
	private $extensionConfig;

	/**
	 * Detected available extensions.
	 * @var array of strings
	 */
	private $availableExtensions = null;
	
	/**
	 * List if extension classes which have had their onLoad() static method called.
	 * @var array of strings 
	 */
	// private static $loadCalledExtensions = array();
	private static $staticLoadedExtensions = array();

	/**
	 * Loaded extensions instances.
	 * @var array of extension objects.
	 */
	private $loadedExtensions = array();
	
	/**
	 * Initialised extension objects.
	 * @var array of Extension objects 
	 */
//	private $initialisedExtensions = array();

	/**
	 * Adds an extension directory.
	 * @param \Kojin\Fs\Directory $dir
	 */
	public function addExtensionDir(Directory $dir) {
		if (is_null($this->availableExtensions) === false) {
			throw new ExtensionLoaderException("Additional extension directories cannot be added once an extension has been loaded.");
		}
		if ($dir->exists() === false) {
			throw new ExtensionLoaderException("Extension directory not found: " . $dir->getPath());
		}
		$this->extensionDirs[] = $dir;
	}

	/**
	 * Sets the extension config.
	 * @param \Kojin\Config\Config $config
	 */
	public function setExtensionConfig(Config $config) {
		$this->extensionConfig = $config;
	}
	
	/**
	 * Returns true if the provided extension is enabled.
	 * @param string $extension
	 * @return bool
	 */
	public function isExtensionEnabled($extension) {
		if (is_null($this->extensionConfig)) {
			return false;
		}
		if (isset($this->extensionConfig->$extension) === false) {
			return false;
		}
		if ($this->extensionConfig->$extension->enabled !== true) {
			return false;
		}
		return true;
	}

	/**
	 * Returns a list of available extensions.
	 * This method does not scan anything, but merely returns a list of the available directories.
	 * @return array
	 */
	public function getAvailableExtensions() {
		if (is_null($this->availableExtensions)) {
			$this->availableExtensions = array();
			// Scan extension directories
			foreach ($this->extensionDirs as $extDir) {
				$glob = glob($extDir->getPath() . DS . "*");
				// Foreach extension
				foreach ($glob as $dir) {
					// Get name
					$realName = basename($dir);
					$name = strtolower($realName);
					// Check for expected main file
					$mainFile = $dir . DS . $realName . "Extension.php";
					if (is_file($mainFile)) {
						// Add extension
						$this->availableExtensions[$name] = array(
							"dir" => $dir,
							"mainFile" => $mainFile
						);
					}
				}
			}
		}
		return $this->availableExtensions;
	}

	/**
	 * Loads all the available and enabled extensions.
	 * @return array of Extension objects
	 */
	public function loadExtensions() {
		$extensions = $this->getAvailableExtensions();
		$loaded = array();
		foreach ($extensions as $extension => $config) {
			try {
				$loaded[$extension] = $this->loadExtension($extension);
			}
			catch (ExtensionNotEnabledException $e) {
				//var_dump($e->getMessage());
				// do nothing
			}
		}
		return $loaded;
	}

	/**
	 * Loads an extension
	 * @param string $extension
	 * @return array the config of the extension
	 */
	public function loadExtension($extension) {
		$kojin = $this->getKojin();

		// Case insensitive
		$extension = strtolower($extension);

		// Check if extension has already been loaded
		if (array_key_exists($extension, $this->loadedExtensions)) {
			return $this->loadedExtensions[$extension];
		}

		// Check if extension is available
		$available = $this->getAvailableExtensions();
		if (array_key_exists($extension, $available) === false) {
			throw new ExtensionNotFoundException($extension);
		}
		// Import load config
		$extensionConfig = $available[$extension];
		$mainFile = $extensionConfig["mainFile"];
		$dir = $extensionConfig["dir"];

		// Check if extension is enabled
		if ($this->isExtensionEnabled($extension) === false) {
			throw new ExtensionNotEnabledException($extension);
		}

		// Get class name
		$classInfo = static::getFileClass($mainFile);
		if (is_null($classInfo["class"])) {
			throw new ExtensionLoaderException("Main file \"$mainFile\" for extension \"$extension\" does not contain a class");
		}
		$class = $classInfo["class"];
		$namespace = $classInfo["namespace"];

		// Include main file
		require_once($mainFile);

		// If this is the first time we're loading the extension, we need to do some extra work
		if (in_array($extension, self::$staticLoadedExtensions) === false) {
			// Register autoloader for extension
			$classLoader = new ClassLoader($namespace, $dir);
			Kojin::registerAutoloader($classLoader);
			
			// Call load event on extension
			call_user_func(array($class, "onLoad"));
			self::$staticLoadedExtensions[] = $extension;
		}

		// Construct extension
		$instance = new $class();
		if ($instance instanceof Extension === false) {
			throw new ExtensionLoaderException("Extension class $class does not extend \Kojin\Extension\Extension");
		}
		// Initialise
		$instance->onInit($kojin);
		
		// Cache object
		$this->loadedExtensions[$extension] = $instance;
		
		return $instance;
	}

	/**
	 * Given a PHP file, returns information about the class defined in that file.
	 * @param string $file
	 * @return array
	 */
	public static function getFileClass($file) {
		// Check file
		if (is_file($file) === false) {
			throw new KojinException("Source file not found: $file");
		}
		// Prepare result
		$class = array(
			"class" => null,
			"namespace" => null
		);
		// Load file
		$source = file_get_contents($file);
		// Parse
		$tokens = token_get_all($source);
		foreach ($tokens as &$token) {
			if (is_array($token) === false) {
				continue;
			}
			$token["name"] = token_name($token[0]);
		}
		
		// Search
		$n = count($tokens);
		for ($i=0 ; $i<$n ; $i++) {
			// Namesapce
			if ($tokens[$i][0] === T_NAMESPACE) {
				// Advance until string
				$j = $i;
				while ($tokens[$j][0] !== T_STRING) {
					$j++;
				}
				// Get namespace
				$namespace = "";
				while(is_string($tokens[$j]) === false) {
					$namespace .= $tokens[$j][1];
					$j++;
				}
				$class["namespace"] = $namespace;
			}
			// Class
			if ($tokens[$i][0] === T_CLASS) {
				// Advance until string
				$j = $i;
				while ($tokens[$j][0] !== T_STRING) {
					$j++;
				}
				// Get class name
				$class["class"] = $tokens[$j][1];

				// Once we have class, we don't care about the rest
				break;
			}
		}

		// Detect root namespace
		if (is_null($class["namespace"])) {
			$class["namespace"] = "\\";
		}
		// Prepend namespace to class
		$class["class"] = $class["namespace"] . "\\" . $class["class"];

		return $class;
	}





	
	/**
	 * Returns the enabled page loaders as per current configuration.
	 * @return array of PageLoader objects
	 * @deprecated
	 */
	public function getPageLoaders() {
		$enabled = $this->loadExtensions();
		$loaders = array();
		foreach ($enabled as $name => $extension) {
			// Check if extension is a PageLoader
			if ($extension instanceof PageLoader) {
				$loaders[] = $extension;
			}
		}
		return $loaders;
	}
	
	/**
	 * Returns the enabled page processors as per current configuration.
	 * @return array of PageProcessor objects
	 * @deprecated
	 */
	public function getPageProcessors() {
		$enabled = $this->loadExtensions();
		$loaders = array();
		foreach ($enabled as $name => $extension) {
			// Check if extension is a PageLoader
			if ($extension instanceof PageProcessor) {
				$loaders[$name] = $extension;
			}
		}
		return $loaders;
	}
	
	/**
	 * Loads all the enabled extensions for the site.
	 * @return array of Extension objects with their names as key
	 * @deprecated
	 */
	// public function getEnabledExtensions() {
	// 	$extensions = array();
	// 	// If we don't have an extension config, no extensions are enabled
	// 	if (is_null($this->extensionConfig)) {
	// 		return $extensions;
	// 	}
	// 	// Get available extensions
	// 	$extensions = $this->getAvailableExtensions();
	// 	// Iterate over extension config and deduce which are enabled
	// 	foreach ($this->extensionConfig as $name => $config) {
	// 		// Check if extension is enabled
	// 		if ($this->isExtensionEnabled($name) !== true) {
	// 			continue;
	// 		}
	// 		// Init and prepare extension
	// 		$extension = $this->loadExtension($name);
	// 		$extension->setConfig($config);
	// 		// Add to list
	// 		$extensions[$name] = $extension;
	// 	}
	// 	return $extensions;
	// }
}
