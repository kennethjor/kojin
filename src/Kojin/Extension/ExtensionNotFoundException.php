<?php

namespace Kojin\Extension;

/**
 * Thrown when the ExcentionLoader was unable to locate the desired extension.
 */
class ExtensionNotFoundException extends ExtensionLoaderException {
	public function __construct($extension) {
		parent::__construct("Extension not found: $extension");
	}
}
