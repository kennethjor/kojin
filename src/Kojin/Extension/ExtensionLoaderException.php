<?php

namespace Kojin\Extension;

class ExtensionLoaderException extends ExtensionException {
	public function __construct($message) {
		parent::__construct($message);
	}
}