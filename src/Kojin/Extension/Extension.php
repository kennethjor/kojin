<?php

namespace Kojin\Extension;

use Kojin\Kojin;
use Kojin\Config\Config;

/**
 * Base class for all extensions.
 */
abstract class Extension {
	/**
	 * Supplied extension config.
	 * @var \Kojin\Config\Config
	 */
	private $config;
	
	/**
	 * Sets the config.
	 * @param \Kojin\Config\Config $config
	 */
	public function setConfig(Config $config) {
		$this->config = $config;
	}
	
	/**
	 * Returns the config.
	 * @return \Kojin\Config\Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/* *************
	EXTENSION EVENTS
	************* */

	/**
	 * Load event.
	 * This is called when the extension is first loaded.
	 * This should only perform things like loading external
	 * libraries or global configuration.
	 * @return void
	 */
	public static function onLoad() {
		// Extend to implement
	}

	/**
	 * Init event.
	 * This is called when the extension is initialised
	 * within a single Kojin instance.
	 * @return void
	 */
	public function onInit(Kojin $kojin) {
		// Extend to implement
	}
}
