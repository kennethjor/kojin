<?php

namespace Kojin\Extension;

use Kojin\Content\Page;

/**
 * Interface for page processors which should be added to both static and dynamic pipelines.
 */
interface PageProcessor {
	/**
	 * Processes the supplied source.
	 * @param \Kojin\Content\Page $page
	 * @param string $html
	 * @return string
	 */
	public function processPage(Page $page, $html);
}
