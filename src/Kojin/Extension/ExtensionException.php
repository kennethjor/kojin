<?php

namespace Kojin\Extension;

/**
 * Generic extension exception.
 */
class ExtensionException extends \Kojin\KojinException {
	public function __construct($message) {
		parent::__construct($message);
	}
}
