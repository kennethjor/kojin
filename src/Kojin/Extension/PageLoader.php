<?php

namespace Kojin\Extension;

use Kojin\Fs\File;

/**
 * Interface for page loaders.
 * Page loaders basically converts some type of source into html.
 */
interface PageLoader {
	/**
	 * Loads content from a file.
	 * @param \Kojin\Fs\File $file
	 * @return string html
	 */
	public function loadFile(File $file);

	/**
	 * Loads content from source.
	 * @param string $source
	 * @return string html
	 */
	public function loadSource($source);
}
