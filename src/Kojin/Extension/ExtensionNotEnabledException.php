<?php

namespace Kojin\Extension;

/**
 * Generic extension exception.
 */
class ExtensionNotEnabledException extends ExtensionException {
	public function __construct($message) {
		parent::__construct($message);
	}
}
