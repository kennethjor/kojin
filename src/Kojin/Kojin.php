<?php

namespace Kojin;

use Kojin\Content\HasSite;
use Kojin\Content\Site;
use Kojin\Extension\ExtensionLoader;
use Kojin\Processor\MarkdownProcessor;
use Kojin\Render\PageRenderer;
use Kojin\Render\PipelineFactory;
use Kojin\Route\Router;
use Kojin\Fs\Directory;

/**
 * Base class for Kojin.
 */
class Kojin {
	use HasSite { setSite as private; }

	/**
	 * The router for this instance.
	 * @var Kojin\Route\Router
	 */
	private $router;

	/**
	 * The extension loader for this instance.
	 * @var Kojin\Extension\ExtensionLoader
	 */
	private $extensionLoader;
	
	/**
	 * Constructor.
	 * @param string $siteRoot the root directory of the site
	 */
	public function __construct($siteRoot) {
		// Create site
		$site = new Site($siteRoot);
		$this->setSite($site);
		$site->setKojin($this);
	}

	/**
	 * Returns the router for this instance.
	 * @return Kojin\Route\Router
	 */
	public function getRouter() {
		if (is_null($this->router)) {
			$router = Router::createRouter();
			$router->setKojin($this);
			$this->router = $router;
		}
		return $this->router;
	}

	/**
	 * Returns an ExtensionLoader configured with Kojin's base extension directory.
	 * @return \Kojin\Extension\ExtensionLoader
	 */
	public function getExtensionLoader() {
		if (is_null($this->extensionLoader)) {
			$loader = new ExtensionLoader();
			$loader->setKojin($this);
			$loader->addExtensionDir(static::getExtensionDir());

			$site = $this->getSite();
			$siteExt = $site->getExtensionDir();
			if ($siteExt->exists()) {
				$loader->addExtensionDir($siteExt);
			}
			$loader->setExtensionConfig($site->getExtensionConfig());

			$this->extensionLoader = $loader;
		}
		return $this->extensionLoader;
	}

	/**
	 * Run method for Kojin.
	 * @return void
	 */
	public function run() {
		$router = $this->getRouter();
		// Load extensions
		$this->getExtensionLoader()->loadExtensions();
		// Initialise site from router
		$this->getSite()->setRootUrl($router->getRootUrl());
		// Route
		$path = $router->getRelativePath();
		$page = $router->route($path);
		// Render
		$renderer = new PageRenderer();
		$renderer->setKojin($this);
		$html = $renderer->renderPage($page);

		echo $html;
	}
	
	/**
	 * Convenience function for registering autoloaders.
	 * @param Kojin\Classloader $loader
	 */
	public static function registerAutoloader(ClassLoader $loader) {
		spl_autoload_register(array($loader, "load"));
	}

	/**
	 * Returns the bundle dirextory.
	 * @return \Kojin\Fs\Directory
	 */
	public static function getBundleDir() {
		return new Directory(KOJIN_ROOT . DS . "bundle");
	}

	/**
	 * Returns the bundled extensions directory.
	 * @return \Kojin\Fs\Directory
	 */
	public static function getExtensionDir() {
		return new Directory(KOJINEXT_SRC);
	}

	/**
	 * Error handler.
	 */
	public static function __error_handler($errno, $errstr, $errfile = null, $errline = null, array $errcontext = null) {
		$errname = "unknown error type";
		switch ($errno) {
			case E_ERROR:
				$errname = "E_ERROR";
				break;
			case E_WARNING:
				$errname = "E_WARNING";
				break;
			case E_PARSE:
				$errname = "E_PARSE";
				break;
			case E_NOTICE:
				$errname = "E_NOTICE";
				break;
			case E_CORE_ERROR:
				$errname = "E_CORE_ERROR";
				break;
			case E_CORE_WARNING:
				$errname = "E_CORE_WARNING";
				break;
			case E_COMPILE_ERROR:
				$errname = "E_COMPILE_ERROR";
				break;
			case E_COMPILE_WARNING:
				$errname = "E_COMPILE_WARNING";
				break;
			case E_USER_ERROR:
				$errname = "E_USER_ERROR";
				break;
			case E_USER_WARNING:
				$errname = "E_USER_WARNING";
				break;
			case E_USER_NOTICE:
				$errname = "E_USER_NOTICE";
				break;
			case E_STRICT:
				$errname = "E_STRICT";
				break;
			case E_RECOVERABLE_ERROR:
				$errname = "E_RECOVERABLE_ERROR ";
				break;
			case E_DEPRECATED:
				$errname = "E_DEPRECATED";
				break;
			case E_USER_DEPRECATED:
				$errname = "E_USER_DEPRECATED";
				break;
		}

		$message = "[$errname] $errstr";

		throw new \ErrorException($message, $errno, 1, $errfile, $errline);
	}
}
