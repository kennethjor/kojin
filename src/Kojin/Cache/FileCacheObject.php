<?php

namespace Kojin\Cache;

use Kojin\Fs\HasFile;
use Kojin\Fs\Directory;

/**
 * An object cached through FileCache.
 */
class FileCacheObject {
	/**
	 * The directory.
	 * @var \Kojin\Fs\Directory
	 */
	private $dir;
	
	/**
	 * The hash for the object.
	 * @var string
	 */
	private $hash;
	
	/**
	 * The set root URL supplied by FileCache, if it had one.
	 * @var string
	 */
	private $rootUrl;
	
	/**
	 * The set prefix.
	 * @var string
	 */
	private $prefix;
	
	/**
	 * The set suffix.
	 * @var string
	 */
	private $suffix;
	
	/**
	 * Constructor.
	 */
	public function __construct(Directory $dir, $hash) {
		if (strlen($hash) < 5) {
			throw new \Kojin\KojinException("Hash must be at least 5 characters long.");
		}
		$this->hash = $hash;
		$this->dir = $dir;
	}
	
	/**
	 * Sets the root URL for URL generation.
	 * @param string $root
	 */
	public function setRootUrl($root) {
		$this->rootUrl = $root;
	}
	
	/**
	 * Sets the prefix.
	 * @var string $prefix
	 */
	public function setPrefix($prefix) {
		$this->prefix = $prefix;
	}
	
	/**
	 * Sets the suffix.
	 * @var string $suffix
	 */
	public function setSuffix($suffix) {
		$this->suffix = $suffix;
	}
	
	/**
	 * Returns the hash.
	 * @return string
	 */
	public function getHash() {
		return $this->hash;
	}
	
	/**
	 * Returns the base path for the object.
	 * @return string
	 */
	public function getBasePath() {
		// Dir parts
		$parts = array(
			substr($this->hash, 0, 2),
			substr($this->hash, 2, 2)
		);
		// File part
		$filepart = substr($this->hash, 4);
		// Prefix
		if (is_null($this->prefix) === false) {
			$filepart = $this->prefix . $filepart;
		}
		// Suffix
		if (is_null($this->suffix) === false) {
			$filepart .= $this->suffix;
		}
		// Add file to parts
		$parts[] = $filepart;
		// Implode
		$base = implode(DS, $parts);
		
		return DS.$base;
	}
	
	/**
	 * Returns the base URL for this object.
	 */
	public function getBaseUrl() {
		return str_replace(DS, "/", $this->getBasePath());
	}
	
	/**
	 * Returns the URL for the object.
	 */
	public function getUrl() {
		if (is_null($this->rootUrl)) {
			return null;
		}
		return $this->rootUrl . $this->getBaseUrl();
	}
	
	/**
	 * Returns a file object for this object.
	 * @return \Kojin\Fs\File
	 */
	public function getFile() {
		return $this->dir->getFile($this->getBasePath());
	}
	
	/**
	 * Returns the content of the cache file.
	 */
	public function get() {
		$file = $this->getFile();
		if ($file->exists() === false) {
			return null;
		}
		return unserialize($file->getContents());
	}
	
	/**
	 * Commits content to the cache file.
	 */
	public function put($object) {
		$file = $this->getFile();
		// Create dir
		$dir = $file->getDir();
		if ($dir->exists() === false) {
			$dir->create();
		}
		$file->putContents(serialize($object));
		return true;
	}
}
