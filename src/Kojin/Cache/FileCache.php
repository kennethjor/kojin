<?php

namespace Kojin\Cache;

use Kojin\Fs\Directory;
use Kojin\Fs\File;

/**
 * Handles caching into files.
 */
class FileCache {
	/**
	 * The directory to use.
	 * @var \Kojin\Fs\Directory
	 */
	private $dir;
	
	/**
	 * Configured URL root for cached objects.
	 * @var string
	 */
	private $rootUrl;
	
	/**
	 * Constructor.
	 * @param \Kojin\Fs\Directory $dir
	 */
	public function __construct(Directory $dir) {
		$this->dir = $dir;
	}
	
	/**
	 * Returns the dir used for caching.
	 * @return \Kojin\Fs\Directory
	 */
	public function getDir() {
		return $this->dir;
	}
	
	/**
	 * Generates a cache id from the supplied value.
	 * Pretty much anything can be supplied, since serialize()
	 * used internally. The only special charecteristic of
	 * this method is the handling of arrays. Before generating
	 * the final ID, arrays are sorted by key, meaning multiple
	 * values can be assembled in an array without having to
	 * worry about the order of the elements.
	 * @param mixed $id
	 * @return string hex
	 */
	public function generateHash($id) {
		// Deal with arrays specially
		if (is_array($id)) {
			ksort($id);
		}
		// Serialize
		$serialized = serialize($id);
		// Hash both sha1 and md5 to make collisions nearly impossible
		$hash = sha1($serialized) . md5($serialized);
		
		return $hash;
	}
	
	/**
	 * Generates and returns a base file path for a cached object given an ID.
	 * @param string $hash
	 * @return string
	 */
	public function getBasePath($hash) {
		if (strlen($hash) < 5) {
			throw new \Kojin\KojinException("ID must be at least 5 characters long");
		}
		$parts = array(
			$this->dir->getPath(),
			substr($hash, 0, 2),
			substr($hash, 2, 2),
			substr($hash, 4)
		);
		$base = implode(DS, $parts);
		return $base;
	}
	
	/**
	 * Returns a CachedFile for a specific hash.
	 * @param string $hash
	 * @param string $prefix
	 * @param string $suffix
	 * @return \Kojin\Cache\CachedObject
	 */
	public function getObject($id, $prefix = null, $suffix = null) {
		$hash = $this->generateHash($id);
		$cached = new FileCacheObject($this->getDir(), $hash);
		// Transfer rootUrl
		if (is_string($this->rootUrl)) {
			$cached->setRootUrl($this->rootUrl);
		}
		// Prefix
		if (is_null($prefix) === false) {
			$cached->setPrefix($prefix);
		}
		// Suffix
		if (is_null($suffix) === false) {
			$cached->setSuffix($suffix);
		}
		
		return $cached;
	}
	
	/**
	 * Commits content to the cache.
	 * @param mixed $id
	 * @param mixed $content
	 * @param string $prefix
	 * @param string $suffix
	 * @return bool
	 */
	public function put($id, $content, $prefix = null, $suffix = null) {
		// Prepare object
		$cached = $this->getObject($id, $prefix, $suffix);
		$cached->put($content);
		
		return $cached;
	}
	
	/**
	 * Returns content from cache by id, or null if it doesn't exist.
	 * @param mixed $id
	 * @param string $prefix
	 * @param string $suffix
	 * @return mixed
	 */
	public function get($id, $prefix = null, $suffix = null) {
		$cached = $this->getObject($id, $prefix, $suffix);
		return $cached->get();
	}
	
	/**
	 * Sets the root URL.
	 * @param string $root
	 */
	public function setRootUrl($root) {
		$this->rootUrl = $root;
	}
	
	/**
	 * Returns the root URL.
	 * @return string
	 */
	public function getRootUrl() {
		return $this->rootUrl;
	}
}
