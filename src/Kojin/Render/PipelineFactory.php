<?php

namespace Kojin\Render;

use Kojin\Extension\ExtensionLoader;

/**
 * Factory for Pipelines.
 */
class PipelineFactory {
	/**
	 * The extension loader to use.
	 * @var \Kojin\Extension\ExtensionLoader
	 */
	private $extensionLoader;
	
	/**
	 * Constructor
	 * @param \Kojin\Extension\extensionLoader $loader
	 */
	public function __construct(ExtensionLoader $loader) {
		$this->extensionLoader = $loader;
	}
	
	/**
	 * Creates a pipeline for page processing.
	 * @return \Kojin\Render\PagePipeline
	 */
	public function createPagePipeline() {
		$pipeline = new PagePipeline();
		$processors = $this->extensionLoader->getPageProcessors();
		$pipeline->addPageProcessors($processors);
		return $pipeline;
	}
}
