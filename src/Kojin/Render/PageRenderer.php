<?php

namespace Kojin\Render;

use Kojin\Content\Page;
use Kojin\HasKojin;

/**
 * Handles the complete rendering of a page.
 */
class PageRenderer {
	use HasKojin;

	/**
	 * Renders a page.
	 * @param \Kojin\Content\Page $page
	 * @return string
	 */
	public function renderPage(Page $page) {
		$kojin = $this->getKojin();
		$site = $kojin->getSite();
		// Load page
		$pageLoader = $page->getPageLoader();
		$html = $pageLoader->loadFile($page->getFile());
		
		// Get layout
		$layout = $page->getLayout();
		// Inject page into layout
		$html = str_replace("[kojin:content]", $html, $layout->getSource());
		
		// Create pipeline
		$extensionLoader = $kojin->getExtensionLoader();
		$extensionLoader->setExtensionConfig($site->getExtensionConfig());
		$pipelineFactory = new PipelineFactory($extensionLoader);
		$pipeline = $pipelineFactory->createPagePipeline();
		
		// Process the page
		$html = $pipeline->processPage($page, $html);
		
		return $html;
	}
}
