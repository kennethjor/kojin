<?php

namespace Kojin\Render;

use \DOMDocument;
use \DOMXpath;

/**
 * Handles the HTML representation of a page while it's rendering.
 * Html gives direct access to the source including handling of DOMDocument and DOMXpath ojects.
 */
class Html {
	/**
	 * The HTML source.
	 * @var string
	 */
	private $source;
	
	/**
	 * The source's DOMDocument.
	 * @var \DOMDocument
	 */
	private $dom;
	
	/**
	 * Constructor.
	 * @param string|\DOMDocument $html
	 */
	public function __construct($html) {
		if (is_string($html)) {
			$this->setSource($html);
		}
		elseif ($html instanceof DOMDocument) {
			$this->setDom($html);
		}
		else {
			throw new \Kojin\KojinException("Unknown source type: " . gettype($html));
		}
	}
	
	/**
	 * Sets the source.
	 * @param string $html
	 */
	public function setSource($html) {
		// If source is the same, this call should be noop
		if (is_null($this->source) === false && $html === $this->source) {
			return;
		}
		$this->source = $html;
		$this->dom = null;
	}
	
	/**
	 * Sets the DOMDocument.
	 * @param \DOMDocument $dom
	 */
	public function setDom(DOMDocument $dom) {
		// If the DOM being set is the same as before, this call should be noop
		if (is_null($this->dom) === false && $dom === $this->dom) {
			return;
		}
		$this->dom = $dom;
		$this->source = null;
	}
	
	/**
	 * Returns the source HTML.
	 * @return string 
	 */
	public function getSource() {
		// If the DOM is already set, we have to rely on it do preserve changes
		if (is_null($this->dom) === false) {
			return $this->dom->saveHTML();
		}
		// DOM is not set, return source
		return $this->source;
	}
	
	/**
	 * Returns the DOM.
	 * @return \DOMDocument 
	 */
	public function getDom() {
		if (is_null($this->dom)) {
			libxml_use_internal_errors(true);
			$this->dom = new DOMDocument("1.0", "utf8");
			$this->dom->loadHTML($this->source);
			libxml_clear_errors();
		}
		return $this->dom;
	}
	
	/**
	 * Queries the DOM for an XPath expression.
	 * @param string $expression
	 * @return \DOMNodeList
	 */
	public function xpath($expression) {
		$xpath = new DOMXpath($this->getDom());
		return $xpath->query($expression);
	}
}
