<?php

namespace Kojin\Render;

use Kojin\Content\Page;
use Kojin\Extension\PageProcessor;

/**
 * The PagePipeline is responsible for running
 * the PageProcessors against a page. 
 */
class PagePipeline {
	/**
	 * Added PageProcessors
	 * @var array of PageProcessors 
	 */
	private $processors = array();
	
	/**
	 * Adds a PageProcessor.
	 * @param string $name the name of the extension supplying the processor
	 * @param \Kojin\Extension\PageProcessor $processor 
	 */
	public function addPageProcessor($name, PageProcessor $processor) {
		$this->processors[$name] = $processor;
	}
	
	/**
	 * Adds multiple PageProcessors
	 * @param array $processors an array of PageProcessor objects
	 */
	public function addPageProcessors(array $processors) {
		foreach ($processors as $name => $processor) {
			$this->addPageProcessor($name, $processor);
		}
	}
	
	/**
	 * Returns the added PageProcessors.
	 * @return array of PageProcessor objects with their names as key 
	 */
	public function getPageProcessors() {
		return $this->processors;
	}
	
	/**
	 * Runs a page through the page processors.
	 * @param \Kojin\Content\Page $page
	 * @param string $html
	 * @return string
	 */
	public function processPage(Page $page, $html) {
		// Import processors
		$processors = $this->getPageProcessors();
		// Keep running the content through all the processors
		// until none of them make any changes.
		while (true) {
			// Create signature
			$sig = sha1($html);
			// Run processors
			foreach ($processors as $processor) {
				$html = $processor->processPage($page, $html);
			}
			// New signatrue and compare
			$newSig = sha1($html);
			if ($sig == $newSig) {
				break;
			}
			$sig = $newSig;
		}
		return $html;
	}
}
