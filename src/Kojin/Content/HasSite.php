<?php

namespace Kojin\Content;

/**
 * Trait to easily attach site acces to a class. 
 */
trait HasSite {
	/**
	 * The site.
	 * @var \Kojin\Content\Site
	 */
	private $trait_hasSite_site;
	
	/**
	 * Sets the site.
	 * @param \Kojin\Content\Site $site 
	 */
	public function setSite(Site $site) {
		$this->trait_hasSite_site = $site;
	}
	
	/**
	 * Returns the site.
	 * @return \Kojin\Content\Site 
	 */
	public function getSite() {
		return $this->trait_hasSite_site;
	}
}

