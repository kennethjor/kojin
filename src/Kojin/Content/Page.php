<?php

namespace Kojin\Content;

//use \Kojin\Processor\MarkdownProcessor;
use Kojin\Fs\File;
use Kojin\Fs\HasFile;
use Kojin\Extension\PageLoader;

/**
 * A `Page` is a piece of content displayed directly in a browser.
 * A `Page` is always wrapped within a layout.
 * If content does not extend `Page`, it will not be wrapped in a layout.
 */
class Page extends AbstractContent {
	use HasSite;
	use HasFile;
	use HasFolder;

	/**
	 * Config for the page.
	 * @var \Kojin\Config\Config
	 */
	private $config;

	/**
	 * The PageLoader for the page.
	 * @var \Kojin\Extension\PageLoader
	 */
	private $pageLoader;

	/**
	 * Constructor.
	 */
	public function __construct($path) {
//		parent::__construct(static::TYPE_FILE);
		$this->setUrlPath($path);
	}

	/**
	 * Returns the page loader for this page.
	 * @return \Kojin\Extension\PageLoader
	 */
	public function getPageLoader() {
		return $this->pageLoader;
	}

	/**
	 * Sets the page loader for this page.
	 * @return \Kojin\Extension\PageLoader
	 */
	public function setPageLoader(PageLoader $loader) {
		$this->pageLoader = $loader;
	}

	/**
	 * Returns the source for the page.
	 * @return string
	 */
	// public function getSource() {
	// 	return file_get_contents($this->getFilePath());
	// }

	/**
	 * Returns the config for the page.
	 * @return \Kojin\Config\Config
	 */
	public function getConfig() {
		if (is_null($this->config)) {
			$base = basename($this->getUrlPath());
			// Try straight base name
			$file = $this->getFolder()->getDir()->getFile($base . ".yaml");
			if ($file->exists() === false) {
				// Try super basic name
				$split = explode(".", $base);
				$file = $this->getFolder()->getDir()->getFile($split[0] . ".yaml");
			}
			$config = $this->getConfigFile($file);
			$this->config = $config;
		}
		return $this->config;
	}

	/**
	 * Returns the layout for this page.
	 * @return \Kojin\Content\Layout
	 */
	public function getLayout() {
		$file = $this->getConfig()->layout;
		if (is_null($file)) {
			return $this->getFolder()->getLayout();
		}
		return $this->getSite()->getLayout($file);
	}
}
