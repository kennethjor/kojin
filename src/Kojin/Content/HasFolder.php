<?php

namespace Kojin\Content;

/**
 * Trait to easily attach folder acces to a class. 
 */
trait HasFolder {
	/**
	 * The folder.
	 * @var \Kojin\Content\Folder 
	 */
	private $trait_hasFolder_folder;
	
	/**
	 * Sets the folder.
	 * @param \Kojin\Content\Folder $folder 
	 */
	public function setFolder(Folder $folder) {
		$this->trait_hasFolder_folder = $folder;
	}
	
	/**
	 * Returns the folder.
	 * @return \Kojin\Content\Folder 
	 */
	public function getFolder() {
		return $this->trait_hasFolder_folder;
	}
}

