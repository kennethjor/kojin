<?php

namespace Kojin\Content;

use Kojin\Config\Config;
use Kojin\Config\YamlConfig;
use Kojin\Route\Router;
use Kojin\Fs\File;

/**
 * Abstract class for content.
 */
abstract class AbstractContent {
	use HasSite;

	/**
	 * The URL path of the content.
	 */
	private $urlPath;

	/**
	 * Returns the URL path of the content.
	 * @return string
	 */
	public function getUrlPath() {
		return $this->urlPath;
	}

	/**
	 * Sets the filesystem path of the content.
	 * @param string $path
	 * @throws \Kojin\Content\NotFoundException
	 */
	public function setUrlPath($path) {
		// Sanitize URL
		$path = Router::sanitizeUrl($path);
		
		$this->urlPath = $path;
	}

	/**
	 * Returns the content config.
	 * @param \Kojin\Fs\File $file the filename of the config file
	 * @return \Kojin\Config\Config
	 */
	protected function getConfigFile(File $file) {
		// Init config
		$config = new YamlConfig();
		// Check for file
		if ($file->exists()) {
			$config->loadFile($file);
		}
		return $config;
	}
	
	/**
	 * Returns the config for this piece of content.
	 * @return \Kojin\Config\Config 
	 */
	public abstract function getConfig();
	
	/**
	 * Returns the extension config for this piece of content.
	 * @return \Kojin\Config\Config
	 * @deprecated
	 */
	// public function getExtensionConfig() {
	// 	$config = $this->getConfig()->extensions;
	// 	if (is_null($config)) {
	// 		return new Config();
	// 	}
	// 	return $config;
	// }
}
