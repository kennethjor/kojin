<?php

namespace Kojin\Content;

use Kojin\Kojin;
use Kojin\HasKojin;
use Kojin\Fs\File;
use Kojin\Fs\Directory;
use Kojin\Fs\HasDirectory;
use Kojin\Route\Router;
use Kojin\Cache\FileCache;

/**
 * Site handler.
 */
class Site extends AbstractContent {
	use HasDirectory;
	use HasKojin;
	
	/**
	 * Base URL for the entire site.
	 * @var string
	 */
	private $baseUrl;

	/**
	 * Site config.
	 * @var \Kojin\Config\Config
	 */
	private $config;
	
	/**
	 * FileCache instance for public caching.
	 * @var \Kojin\Cache\FileCache
	 */
	private $publicCache;

	/**
	 * Constructor
	 * @param string $dir the path to the directory root of the site.
	 */
	public function __construct($dir) {
		$this->setDir($dir);
		if ($this->getDir()->exists() === false) {
			throw new NotFoundException($dir);
		}
	}

	/**
	 * Returns the site config.
	 * @return \Kojin\Config\Config
	 */
	public function getConfig() {
		if (is_null($this->config)) {
			$default = parent::getConfigFile(new File(dirname(__FILE__) . DS . "default-site.yaml"));
			$site = parent::getConfigFile(new File($this->getDir()->getPath() . DS . "site.yaml"));
			$this->config = $default->extend($site);
		}
		return $this->config;
	}
	
	/**
	 * Returns the extension config for this site.
	 * @return \Kojin\Config\Config
	 */
	public function getExtensionConfig() {
		$config = $this->getConfig()->extensions;
		if (is_null($config)) {
			return new Config();
		}
		return $config;
	}

	/**
	 * Returns the configured site name.
	 * @return string
	 */
	public function getTitle() {
		return $this->getConfig()->title;
	}

	/**
	 * Returns the base content directory.
	 * @return \Kojin\Fs\Directory
	 */
	public function getContentDir() {
		return new Directory($this->getDir()->getPath() . DS . "content");
	}

	/**
	 * Returns the base layout directory.
	 * @return \Kojin\Fs\Directory
	 */
	public function getLayoutDir() {
		return new Directory($this->getDir()->getPath() . DS . "layout");
	}
	
	/**
	 * Returns the base extension directory.
	 * @return \Kojin\Fs\Directory
	 */
	public function getExtensionDir() {
		return new Directory($this->getDir()->getPath() . DS . "extensions");
	}
	
	/**
	 * Returns the public directory.
	 * @return \Kojin\Fs\Directory
	 */
	public function getPublicDir() {
		return new Directory($this->getDir()->getPath() . DS . "public");
	}

	/**
	 * Returns a Page by path.
	 * @param string $path
	 * @return \Kojin\Content\Page
	 */
	public function getPage($path) {
		$path = Router::sanitizeUrl($path);
		// Prepare page
		$page = new Page($path);
		$page->setSite($this);
		// Handle root
		if ($path == "/") {
			$page->setFolder($this->getFolder("/"));
		}
		// Handle sub folder
		else {
			$page->setFolder($this->getFolder(dirname($path)));
		}

		return $page;
	}

	/**
	 * Returns a Layout by path.
	 * @param string $path
	 * @return \Kojin\Content\Layout
	 */
	public function getLayout($path = null) {
		// Get configured layout
		if (is_null($path)) {
			$path = $this->getConfig()->layout;
		}
		// Prepare and check layout file
		$layoutPath = $this->getLayoutDir()->getPath() . DS . $path;
		$layoutFile = new File($layoutPath);
		if ($layoutFile->exists() === false) {
			throw new NotFoundException($layoutFile->getPath());
		}
		// Prepare layout
		$layout = new Layout($layoutFile);
		$layout->setSite($this);
		
		return $layout;
	}
	
	/**
	 * Returns a folder inside the site.
	 * @param string $path the folder path
	 * @return \Kojin\Content\Folder
	 */
	public function getFolder($path) {
		$path = Router::sanitizeUrl($path);
		// Prepare directory
		$dir = new Directory($this->getContentDir()->getPath() . DS . $path);
		if ($dir->exists() === false) {
			throw new NotFoundException($path);
		}
		// Prepare folder
		$folder = new Folder($path);
		$folder->setSite($this);
		$folder->setDir($dir);
		// If this is not the root, prepare parent
		if ($path !== "/") {
			$folder->setFolder($this->getFolder(dirname($path)));
		}
		return $folder;
	}

	/**
	 * Returns an ExtensionLoader for this Site.
	 * @return \Kojin\Extension\ExtensionLoader
	 */
	// public function getExtensionLoader() {
	// 	$loader = Kojin::getExtensionLoader();
	// 	// Set site
	// 	$loader->setSite($this);
	// 	// Add site's extension directory
	// 	$extensionDir = $this->getExtensionDir();
	// 	if ($extensionDir->exists()) {
	// 		$loader->addExtensionDir($extensionDir->getPath());
	// 	}
	// 	$loader->setExtensionConfig($this->getExtensionConfig());
		
	// 	return $loader;
	// }

	/**
	 * Sets the root URL for the site.
	 * @param string $baseUrl
	 * @return \Kojin\Site
	 */
	public function setRootUrl($baseUrl) {
		$this->baseUrl = $baseUrl;
		return $this;
	}

	/**
	 * Returns the root URL for the site.
	 * @return string
	 */
	public function getRootUrl() {
		return $this->baseUrl;
	}
	
	/**
	 * Returns a FileCache object for use with public caching.
	 * @return \Kojin\Cache\FileCache
	 */
	public function getPublicCache() {
		if (is_null($this->publicCache)) {
			// Get and create cache dir
			$cacheDir = $this->getPublicDir()->getDir("cache");
			if ($cacheDir->exists() === false) {
				$cacheDir->create();
			}
			$this->publicCache = new FileCache($cacheDir);
			$this->publicCache->setRootUrl($this->getRootUrl() . "/cache");
		}
		return $this->publicCache;
	}
}
