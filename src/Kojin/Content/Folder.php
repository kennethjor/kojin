<?php

namespace Kojin\Content;

use \Kojin\Fs\HasDirectory;

/**
 * A content folder.
 */
class Folder extends AbstractContent {
	use HasDirectory;
	use HasFolder;

	/**
	 * Config for the folder.
	 * @var \Kojin\Config\Config
	 */
	private $config;
	
	/**
	 * Constructor. 
	 */
	public function __construct($path) {
		$this->setUrlPath($path);
	}

	/**
	 * Returns the config for the folder.
	 * @return \Kojin\Config\Config
	 */
	public function getConfig() {
		if (is_null($this->config)) {
			$file = $this->getDir()->getFile("folder.yaml");
			$config = $this->getConfigFile($file);
			$this->config = $config;
		}
		return $this->config;
	}

	/**
	 * Returns the layout for this page.
	 * @return \Kojin\Content\Layout
	 */
	public function getLayout() {
		$file = $this->getConfig()->layout;
		if (is_null($file)) {
			return $this->getSite()->getLayout();
		}
		return $this->getSite()->getLayout($file);
	}
}
