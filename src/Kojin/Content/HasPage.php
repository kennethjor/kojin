<?php

namespace Kojin\Content;

/**
 * Trait to easily attach page acces to a class. 
 */
trait HasPage {
	/**
	 * The page.
	 * @var \Kojin\Content\Page
	 */
	private $trait_hasPage_page;
	
	/**
	 * Sets the page.
	 * @param \Kojin\Content\Page $folder 
	 */
	public function setPage(Page $page) {
		$this->trait_hasPage_page = $page;
	}
	
	/**
	 * Returns the page.
	 * @return \Kojin\Content\Page 
	 */
	public function getPage() {
		return $this->trait_hasPage_page;
	}
}
