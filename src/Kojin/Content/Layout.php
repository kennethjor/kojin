<?php

namespace Kojin\Content;

use \Kojin\Fs\File;
use \Kojin\Fs\HasFile;

/**
 * A layout.
 */
class Layout /*extends AbstractContent*/ {
	use HasSite;
	use HasFile;

	/**
	 * Constructor
	 * @param \Kojin\Content\Site $site the site
	 * @param \Kojin\Fs\File $file the layout file
	 */
	public function __construct(File $file) {
		$this->setFile($file);
	}

	/**
	 * Returns the source of the layout.
	 * @return string
	 */
	public function getSource() {
		return file_get_contents($this->getFile()->getPath());
	}
}
