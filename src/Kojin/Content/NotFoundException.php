<?php

namespace Kojin\Content;

/**
 * Thrown when content is not found.
 */
class NotFoundException extends \Kojin\KojinException {
	public function __construct($resource) {
		parent::__construct("\"$resource\" not found");
	}
}
