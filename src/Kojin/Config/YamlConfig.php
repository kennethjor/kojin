<?php

namespace Kojin\Config;

use Kojin\Kojin;
use Kojin\Fs\File;

/**
 * Yaml parser.
 */
class YamlConfig extends Config {
	/**
	 * Cosntructor.
	 * @param string|array $yaml the yaml source to use, or an array of config elements
	 */
	public function __construct($yaml = null) {
		if (is_string($yaml)) {
			$this->loadSource($yaml);
		}
		else if (is_array($yaml)) {
			parent::__construct($yaml);
		}
	}

	/**
	 * Spyc instance.
	 * @var \Spyc
	 */
	private static $spyc;

	/**
	 * Returns the spyc instance.
	 */
	private static function getSpyc() {
		if (is_null(static::$spyc)) {
			if (class_exists("\Spyc", false) === false) {
				require_once(Kojin::getBundleDir()->getPath() . DS . "spyc" . DS . "spyc.php");
			}
			static::$spyc = new \Spyc();
		}
		return static::$spyc;
	}

	/**
	 * Loads YAML from string.
	 * @param string $yaml
	 */
	public function loadSource($yaml) {
		$vars = static::getSpyc()->load($yaml);
		$this->setVars($vars);
	}

	/**
	 * Loads YAML from file.
	 * @param \Kojin\Fs\File $file
	 */
	public function loadFile(File $file) {
		if ($file->exists() === false) {
			throw new ConfigException("Config file not found: " . $file->getPath());
		}
		$yaml = $file->getContents();
		$this->loadSource($yaml);
	}
}
