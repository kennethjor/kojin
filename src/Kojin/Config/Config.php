<?php

namespace Kojin\Config;

/**
 * Base hierarchichal config handler.
 */
class Config implements \ArrayAccess, \Countable, \IteratorAggregate {
	/**
	 * The loaded variables.
	 * @var array
	 */
	private $data = array();

	/**
	 * True if config is associative, false if it's numeric, null if neither has been detected.
	 * @var bool|null
	 */
	private $assoc = null;

	/**
	 * Constructor.
	 * @param array $vars
	 */
	public function __construct(array $vars = null) {
		if (is_array($vars)) {
			$this->setVars($vars);
		}
	}

	/**
	 * Sets the vars.
	 * @param array $vars
	 */
	public function setVars(array $vars) {
		$this->data = array();
		// Convert to config objects
		foreach ($vars as $key => $val) {
			$this->__set($key, $val);
		}
	}

	/**
	 * Returns a new Config object with the values from $this extended by $other.
	 * @param \Kojin\Config\Config $other the entending config
	 * @return \Kojin\Config\Config
	 */
	public function extend(Config $other) {
		// If either is empty, copy the other
		if ($this->isEmpty()) {
			return new Config($other->toArray());
		}
		if ($other->isEmpty()) {
			return new Config($this->toArray());
		}

		// Check key types
		if (($this->isNumeric() ^ $other->isNumeric()) || ($this->isAssociative() ^ $other->isAssociative())) {
			throw new ConfigException("Mixing numeric and associative values is not allowed.");
		}

		// Associative extend - replace
		if ($this->isAssociative()) {
			if ($other->isAssociative() === false) {
				throw new \Kojin\KojinException("this should never happen.");
			}
			// Prepare extended config
			$extended = new Config();
			// Prepare key list
			$keys = array_merge($this->getKeys(), $other->getKeys());
			$keys = array_unique($keys);
			// Iterate
			foreach ($keys as $key) {
				// If value doesn't exist in this, add it
				if (isset($this->$key) === false) {
					$extended->$key = $other->$key;
				}
				// If value doesn't exist in other, preserve it
				elseif (isset($other->$key) === false) {
					$extended->$key = $this->$key;
				}
				// Value exists in both
				else {
					// Import values
					$thisVal = $this->$key;
					$otherVal = $other->$key;
					// If both are config objects, extend
					if ($thisVal instanceof self && $otherVal instanceof self) {
						$extended->$key = $thisVal->extend($otherVal);
					}
					// Otherwise replace
					else {
						$extended->$key = $otherVal;
					}
				}
			}

			return $extended;
		}

		// Numeric extend, combine
		else {
			if ($other->isNumeric() === false) {
				throw new \Kojin\KojinException("this should never happen.");
			}
			$thisVals = $this->toArray();
			$otherVals = $other->toArray();
			$combinedVals = array_merge($thisVals, $otherVals);
			$combinedVals = array_unique($combinedVals);
			return new Config($combinedVals);
		}

		throw new \Kojin\KojinException("this should never happen.");
	}

	/**
	 * Converts the config object to an array.
	 * @return array
	 */
	public function toArray() {
		$a = array();
		foreach ($this as $key => $val) {
			if ($val instanceof self) {
				$val = $val->toArray();
			}
			$a[$key] = $val;
		}
		return $a;
	}

	/**
	 * Returns the keys in the config.
	 * @return array
	 */
	public function getKeys() {
		return array_keys($this->data);
	}

	/**
	 * Returns true if the config elements are numeric, false if not.
	 * @return bool
	 */
	public function isNumeric() {
		return $this->assoc === false;
	}

	/**
	 * Returns true if the config elements are associative, false if not.
	 * @return bool
	 */
	public function isAssociative() {
		return $this->assoc === true;
	}

	/**
	 * Returns true if this config is empty.
	 */
	public function isEmpty() {
		return count($this->data) === 0;
	}

	/**
	 * Convenience method for returning hierarchical values.
	 * @param $path string dot-separated JSON-like expression.
	 */
	public function get($path) {
		// Type and sanitasion
		if (is_string($path) === false) {
			return null;
		}
		$path = preg_replace("/\s/", "", $path);
		if (strlen($path) === 0) {
			return null;
		}
		// Split
		$path = explode(".", $path);
		// Iterate
		$current = $this;
		do {
			// We can only pull values from config objects
			if ($current instanceof self === false) {
				return null;
			}
			// Pull
			$current = $current->__get(array_shift($path));
		} while (count($path) > 0);

		return $current;
	}

	/* ***********
	Magic members.
	*********** */
	
	/**
	 * Magic var getter.
	 */
	public function __get($key) {
		if (array_key_exists($key, $this->data)) {
			return $this->data[$key];
		}
		return null;
	}

	/**
	 * Magic var setter.
	 */
	public function __set($key, $val) {
		// Detect key type
		if (is_null($this->assoc)) {
			if (is_string($key)) {
				$this->assoc = true;
			}
			else {
				$this->assoc = false;
			}
		}
		// Validate key type
		if ($this->assoc ^ is_string($key) === true) {
			throw new ConfigException("Mixing numeric and associative values is not allowed.");
		}
		// Check for sub-arrays
		if (is_array($val)) {
			$val = new Config($val);
		}
		$this->data[$key] = $val;
	}

	/**
	 * Returns true if the supplied key exists.
	 * The internal implementation is array_key_exists(), not isset().
	 */
	public function __isset($key) {
		return array_key_exists($key, $this->data);
	}

	/* *********
	ArrayAccess.
	********* */
	public function offsetExists($key) {
		return $this->__isset($key);
	}
	public function offsetGet($key) {
		return $this->__get($key);
	}
	public function offsetSet($key, $val) {
		return $this->__set($key, $val);
	}
	public function offsetUnset($key) {
		if ($this->offsetExists($key)) {
			unset($this->data[$key]);
		}
	}

	/* *******
	Countable.
	******* */
	public function count() {
		return count($this->data);
	}

	/* ***************
	IteratorAggregate.
	*************** */
	public function getIterator() {
		return new \ArrayIterator($this->data);;
	}
}
