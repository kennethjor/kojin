<?php

namespace Kojin\Config;

class ConfigException extends \Kojin\KojinException {
	public function __construct($message) {
		parent::__construct($message);
	}
}