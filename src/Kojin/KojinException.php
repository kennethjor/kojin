<?php

namespace Kojin;

/**
 * Generic Kojin exception.
 */
class KojinException extends \Exception {
	public function __construct($message) {
		parent::__construct($message);
	}
}
