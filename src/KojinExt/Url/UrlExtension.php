<?php

namespace KojinExt\Url;

use Kojin\Extension\Extension;
use Kojin\Extension\PageProcessor;
use Kojin\Content\Site;
use Kojin\Content\Page;

/**
 * Url handler extension.
 */
class UrlExtension extends Extension implements PageProcessor {
	public $tags = array("a", "link", "script", "img");
	public $attrs = array("src", "href");

	/**
	 * The currently processing page.
	 * We must store this while processing because preg_replace_callback
	 * doesn't handle arbitrary arguments.
	 * @var \Kojin\Content\Page 
	 */
	private $page;
	
	/**
	 * Processes the supplied content in a dynamic pipeline.
	 * @param \Kojin\Content\Page $page
	 * @param string $html
	 */
	public function processPage(Page $page, $html) {
		// Save page
		$this->page = $page;
		// Run replace
		$count = 0;
		$html = preg_replace_callback($this->getRegex(), array($this, "replaceCallback"), $html, -1, $count);
		// Reset page to make sure we don't accidentally use it elsewhere
		$this->page = null;

		return $html;
	}

	public function replaceCallback(array $matches) {
		$orig = $matches[0];
		// Detect
		if (array_key_exists("url", $matches) === false) {
			return $orig;
		}
		// Import
		$url = $matches["url"];
		// Replace
		return str_replace($url, $this->processUrl($this->page, $url), $orig);
	}

	/**
	 * Returns the regex for url processing.
	 */
	public function getRegex() {
		// Brackets
		$pre = "<\w*";
		$suf = "(?:>|\/>)";
		// URL match
		$url = "[\\\"\\'](?<url>[^\\\"\\']*)[\\\"\\']";
		// Tag and attr
		$tags = "(?:" . implode("|", $this->tags) . ")";
		$attrs = "(?:" . implode("|", $this->attrs) . ")";
		// Assemple
		$regex = "/" . $pre.$tags."[^<>]*".$attrs."=".$url."[^<>]*".$suf . "/";
		
		return $regex;
	}

	/**
	 * Processes a single URL.
	 * @param \Kojin\Content\Page $page
	 * @param string $url
	 * @return string
	 */
	public function processUrl(Page $page, $url) {
		$site = $page->getSite();
		$folder = $page->getFolder();

		$url = trim($url);
		// Ignore zero-length
		if (strlen($url) === 0) {
			return $url;
		}
		// Ignore URLs with protocol part
		if (preg_match("/[a-zA-Z]:\/\//", $url)) {
			return $url;
		}
		// Site relative
		if (substr($url, 0, 2) === "//") {
			$url = $site->getRootUrl() . "/" . substr($url, 2);
		}
		// Page relative
		else if (substr($url, 0, 1) !== "/" && substr($url, 0, 1) !== "[") {
			// Get full url to page
//			$base = $site->getRootUrl() . "/" . $page->getUrlPath();
			$base = $site->getRootUrl() . "/" . $folder->getUrlPath();
			// Get page dir
//			$base = dirname($base);
			// Add url
			$url = $base . "/" . $url;
		}
		// No match, noop
		else {
			return $url;
		}
		// Remove double slashes
		$url = preg_replace("/\/{2,}/", "/", $url);
		// Remove trailing slash
		if (substr($url, -1) === "/") {
			$url = substr($url, 0, -1);
		}

		return $url;
	}
}
