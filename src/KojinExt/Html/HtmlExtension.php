<?php

namespace KojinExt\Html;

use Kojin\Extension\Extension;
use Kojin\Extension\PageLoader;
use Kojin\Fs\File;
use Kojin\Kojin;
use Kojin\Route\FileTypeRoute;

/**
 * HTML extension.
 */
class HtmlExtension extends Extension implements PageLoader {
	/**
	 * Init event.
	 * @return void
	 */
	public function onInit(Kojin $kojin) {
		// Register route
		$route = new FileTypeRoute();
		$mimetype = "text/html";
		$route
			->addFileType("htm",  $mimetype)
			->addFileType("html", $mimetype);
		$kojin->getRouter()->addRoute("html", $route, "html");
	}
	
	/**
	 * Loads content from a file.
	 * @param \Kojin\Fs\File $file
	 * @return string html
	 */
	public function loadFile(File $file) {
		return $this->loadSource($file->getContents());
	}

	/**
	 * Loads content from source.
	 * @param string $source
	 * @return string html
	 */
	public function loadSource($source) {
		return $source;
	}
}
