<?php

namespace KojinExt\Varsub;

use Kojin\Extension\Extension;
use Kojin\Extension\PageProcessor;
use Kojin\Content\Page;

/**
 * Variable substitution extension.
 */
class VarsubExtension extends Extension implements PageProcessor {
	/**
	 * The currently processing page.
	 * We must store this while processing because preg_replace_callback
	 * doesn't handle arbitrary arguments.
	 * @var \Kojin\Content\Page 
	 */
	private $page;
	
	/**
	 * Processes the supplied content in a dynamic pipeline.
	 * @param \Kojin\Content\Page $page
	 * @param string $html
	 */
	public function processPage(Page $page, $html) {
		// Save page
		$this->page = $page;
		// Run replace
		$count = 0;
		$html = preg_replace_callback($this->getRegex(), array($this, "replaceCallback"), $html, -1, $count);
		// Reset page to make sure we don't accidentally use it elsewhere
		$this->page = null;

		return $html;
	}

	public function replaceCallback(array $matches) {
		$orig = $matches[0];
		// Detect 
		if (array_key_exists("namespace", $matches) === false) {
			return $orig;
		}
		if (array_key_exists("var", $matches) === false) {
			return $orig;
		}
		// Import
		$namesapce = $matches["namespace"];
		$var = $matches["var"];
		$page = $this->page;
		$site = $page->getSite();
		// Namespace
		switch ($namesapce) {
			// Site
			case "site":
				// Special
				if ($var == "rootUrl") {
					return $site->getRootUrl();
				}
				// Config
				$val = $site->getConfig()->get($var);
				if (is_null($val) === false) {
					return $val;
				}

				break;
			// Page
			case "page":
				// Config
				$val = $page->getConfig()->get($var);
				if (is_null($val) === false) {
					return $val;
				}
		}
		return $orig;
	}

	/**
	 * Returns the substition regex.
	 * @return regex
	 */
	public function getRegex() {
		$namespace = "[a-zA-Z]+";
		$var = "[a-zA-Z]+(?:\.[a-zA-Z]+)*";
		$regex = "/\[kojin:(?<namespace>$namespace):(?<var>$var)\]/";
		return $regex;
	}
}
