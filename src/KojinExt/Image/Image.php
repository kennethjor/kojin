<?php

namespace KojinExt\Image;

use Kojin\Fs\File;
use Kojin\Fs\HasFile;
use Imagick;

/**
 * An image.
 */
class Image {
	use HasFile {
		setFile as private traitSetFile;
	}
	
	/**
	 * @var \Imagick
	 */
	private $imagick;
	
	/**
	 * Constructor.
	 * @param \Kojin\Fs\File $file
	 */
	public function __construct(File $file) {
		$this->setFile($file);
	}
	
	/**
	 * Destructor.
	 */
	public function __destroy() {
		if (is_null($this->imagick) === false) {
			$this->imagick->clear();
			$this->imagick->destroy();
		}
	}
	
	/**
	 * Sets the file.
	 * @param \Kojin\Fs\File $file
	 */
	public function setFile(File $file) {
		$this->traitSetFile($file);
		$this->__destroy();
		$this->imagick = new Imagick();
		$this->imagick->readImage($file->getPath());
	}
	
	/**
	 * Returns the Imagick object.
	 * @return \Imgick
	 */
	public function getImagick() {
		return $this->imagick;
	}
	
	/**
	 * Returns the signature of the image.
	 * @return string
	 */
	public function getSignature() {
		return $this->imagick->getImageSignature();
	}
	
	/**
	 * Returns the width of the image.
	 * @return int
	 */
	public function getWidth() {
		return $this->imagick->getImageWidth();
	}
	
	/**
	 * Returns the height of the image.
	 * @return int
	 */
	public function getHeight() {
		return $this->imagick->getImageHeight();
	}
	
	/**
	 * scales the image to the supplied dimensions.
	 * @param int $width
	 * @param int $height
	 */
	public function scale($width, $height) {
		$this->getImagick()->scaleImage($width, $height);
	}

	/**
	 * Crops the image to the desired dimensions.
	 * @param int $width
	 * @param int $height
	 * @param int $x
	 * @param int $y
	 */
	public function crop($width, $height, $x, $y) {
		$this->getImagick()->cropImage($width, $height, $x, $y);
	}
	
	/**
	 * Saves an image.
	 * Currently all images are saves as PNG.
	 * @param \Kojin\Fs\File $file
	 */
	public function save(File $file) {
		if ($file->getExtension() !== "png") {
			throw new \Kojin\KojinException("Only PNG saves are supported.");
		}
		$file->getDir()->create();
		$imagick = $this->getImagick();
		$imagick->setFormat("PNG");
		$imagick->writeImage($file->getPath());
	}
}
