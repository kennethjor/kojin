<?php

namespace KojinExt\Image;

use Kojin\Extension\Extension;
use Kojin\Extension\PageProcessor;
use Kojin\Fs\Directory;
use Kojin\Fs\File;
use Kojin\Content\Site;
use Kojin\Content\Page;
use Kojin\Render\Html;
use Kojin\Kojin;
use Kojin\Route\FileTypeRoute;

use KojinExt\Image\Filter\ImageFilter;
use KojinExt\Image\Filter\ScaleFilter;
use KojinExt\Image\Filter\CropFilter;
use KojinExt\Image\Filter\GrayscaleFilter;

/**
 * The image handler extension.
 */
class ImageExtension extends Extension implements PageProcessor {
	/**
	 * Registered filters.
	 * @var array
	 */
	private $filters = array();

	/**
	 * Load event.
	 * @return void
	 */
	public function onInit(Kojin $kojin) {
		// Register built in filters
		$this->registerFilter("scale", new ScaleFilter());
		$this->registerFilter("crop", new CropFilter());
		$grayscale = new GrayscaleFilter();
		$this->registerFilter("grayscale", $grayscale);
		$this->registerFilter("greyscale", $grayscale);

		// Register routes
		// $route = new FileTypeRoute();
		// $route
		// 	->addFileType("gif",  "image/gif")
		// 	->addFileType("png",  "image/png")
		// 	->addFileType("jpg",  "image/jpeg")
		// 	->addFileType("jpeg", "image/jpeg");
		// $kojin->getRouter()->addRoute("image", $route, "ImageHandler");
	}

	/**
	 * Registers a filter class.
	 */
	public function registerFilter($name, ImageFilter $instance) {
		$this->filters[$name] = $instance;
	}

	/**
	 * Processes the supplied content in a dynamic pipeline.
	 * @param \Kojin\Content\Page $page
	 * @param string $html
	 * @return string
	 */
	public function processPage(Page $page, $html) {
		$site = $page->getSite();
		$cache = $site->getPublicCache();
		$root = $site->getRootUrl();
		$contentDir = $site->getContentDir();
		$html = new Html($html);
		
		// Select image elements present in the content
		$nodes = $html->xpath("//img[@src]");
		foreach ($nodes as $node) {
			// Load source attribute
			$src = $node->getAttribute("src");
			// Get image file
			$file = $this->getImageFile($site, $src);
			// Check if file exists
			if (is_null($file) || $file->exists() === false) {
				continue;
			}

			// Load filters from node
			$filters = $node->getAttribute("filters");
			$node->removeAttribute("filters");

			// Append scale filters from attributes
			$width = intval($node->getAttribute("width"));
			$height = intval($node->getAttribute("height"));
			if ($width > 0 && $height > 0) {
				$filters .= ";scale(width=$width,height=$height)";
			}
			elseif ($width > 0) {
				$filters .= ";scale(width=$width)";
			}
			elseif ($height > 0) {
				$filters .= ";scale(height=$height)";
			}
			if (substr($filters, 0, 1) == ";") {
				$filters = substr($filters, 1);
			}
			// Parse filters
			$filters = $this->parseFiltersString($filters);

			// Create cache ID
			$id = array(
				"originalSignature" => $file->getSha1Hash(),
				"fitlers" => $filters
			);
			// Check cache
			$cached = $cache->getObject($id, null, ".png");
			$cachedFile = $cached->getFile();
			// If cached image does not exist, create it
			$image = null;
			if ($cachedFile->exists() === false) {
				// Load image
				$image = $this->loadImage($file);
				// Run filters
				$this->filterImage($image, $filters);
				// Save image
				$image->save($cachedFile);
			}
			// Otherwise, load cached image
			else {
				$image = $this->loadImage($cachedFile);
			}
			
			// Modify element with new url and size
			$node->setAttribute("src", $cached->getUrl());
			$node->setAttribute("width", $image->getWidth());
			$node->setAttribute("height", $image->getHeight());
		}
		
		return $html->getSource();
	}

	/**
	 * If the provided URL is inside the supplied site and the file exists,
	 * the real path of the image will be returned.
	 * @return \KojinExt\Fs\File|null
	 */
	public function getImageFile(Site $site, $url) {
		$root = $site->getRootUrl();
		// Check if source is within the site root
		if (substr($url, 0, strlen($root)) !== $root) {
			return null;
		}
		$url = substr($url, strlen($root));
		// Prepare file and check if it exists in the content dir
//		$file = new File($site->getContentDir()->getPath() . DS . str_replace("/", DS, $url));
		$file = $site->getContentDir()->getFile(str_replace("/", DS, $url));

		return $file;
	}
	
	/**
	 * Loads an image from a file.
	 * @return \KojinExt\ImageHandler\Image|null
	 */
	public function loadImage(File $file) {
		// Load image
		if ($file->exists()) {
			return new Image($file);
		}
		return null;
	}
	
	/**
	 * Runs filters against a supplied image.
	 * @param \KojinExt\ImageHandler\Image $filters
	 * @param array $fitlers
	 * @todo this should be refactored out into object so other extensions can specify custom filters
	 */
	public function filterImage(Image $image, array $filters) {
		foreach ($filters as $i => $filter) {
			// Verify type
			if (is_array($filter) === false) {
				throw new \Kojin\KojinException("Each filter must be an array, " . gettype($fitler) . " supplied for index $i");
			}
			if (array_key_exists("filter", $filter) === false) {
				throw new \Kojin\KojinException("Filter did not specify type in index $i");
			}
			// Import filter
			$name = $filter["filter"];
			if (array_key_exists($name, $this->filters) === false) {
				throw new ImageHandlerException("Unknown filter: $name");
			}
			$filterInstance = $this->filters[$name];
			// Execute
			$args = null;
			if (array_key_exists("arguments", $filter)) {
				$args = $filter["arguments"];
			}
			$filterInstance->execute($image, $args);
		}
	}

	/**
	 * Parses a string supplied from the filters attribute of images.
	 * Note that while this is implemented, the attributes is not actually
	 * observed by this extension. Currently for more details see the
	 * unit test for this class.
	 * This is probably not written in hte most optimal way possible.
	 * @param string $string
	 * @return array
	 */
	public function parseFiltersString($string) {
		// Construct regexes
		$unnamedArgReg = "[a-zA-Z0-9%\.]+";
		$namedArgReg = "[a-zA-Z0-9]+\h*=\h*$unnamedArgReg";
		$argReg = "(?:$unnamedArgReg|$namedArgReg)";
		$argListReg = "$argReg\h*(?:\h*,\h*$argReg)*";
		$filterNameReg = "(?<filter>[a-zA-Z0-9]+)";
		$filterReg = "^$filterNameReg\h*(?:\(\h*(?<args>$argListReg)?\h*\))?\h*;*";
		// Prepare container
		$filters = array();
		// Match until the end
		$string = trim($string);
		while (strlen($string) > 0) {
			$n = preg_match("/$filterReg/", $string, $matches);
			// Check for errors
			if ($n === 0) {
				throw new ImageHandlerException("\"$string\" is not a valid filter string");
			}
			// Construct base array
			$filter = array("filter" => $matches["filter"]);
			// Attach arguments
			if (array_key_exists("args", $matches)) {
				$args = explode(",", $matches["args"]);
				$argList = array();
				foreach ($args as $k => $v) {
					$v = trim($v);
					// Unnamed arg
					if (preg_match("/^$unnamedArgReg$/", $v)) {
						$argList[] = $v;
					}
					// Named arg
					elseif (preg_match("/^$namedArgReg$/", $v)) {
						$a = explode("=", $v);
						$argList[trim($a[0])] = trim($a[1]);
					}
					// Unknown
					else {
						throw new ImageHandlerException("Unknown argument value: \"$v\"");
					}
				}
				// Filter
				foreach ($argList as $k => $v) {
					// Percent
					if (substr($v, -1) === "%") {
						$argList[$k] = floatval($v) / 100;
					}
				}
				// Add to filter
				$filter["arguments"] = $argList;
			}
			// Remove match from string
			$string = substr($string, strlen($matches[0]));
			$string = trim($string);
			// Add to list
			$filters[] = $filter;
		}
		return $filters;
	}
}
