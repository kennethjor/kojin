<?php

namespace KojinExt\Image\Filter;

use KojinExt\Image\ImageException;
use KojinExt\Image\Image;

/**
 * Filter used for simple scaling of images.
 */
class ScaleFilter implements ImageFilter {
	public function execute(Image $image, array $args = null) {
		// Final size
		$width = null;
		$height = null;
		// Image size
		$orgWidth = $image->getWidth();
		$orgHeight = $image->getHeight();
		// Import args
		if (is_array($args)) {
			// Single args
			if (count($args) == 1) {
				$size = null;
				// Relative resize (percentages)
				if (array_key_exists(0, $args) && $args[0] > 0 && $args[0] < 1) {
					$size[0] = round($orgWidth * $args[0]);
					$size[1] = round($orgHeight * $args[0]);
				}
				// Min size
				else if (array_key_exists("min", $args)) {
					if ($orgWidth < $orgHeight) {
						$args["width"] = $args["min"];
					}
					else {
						$args["height"] = $args["min"];
					}
					unset($args["min"]);
				}
				// Max size
				else if (array_key_exists("max", $args)) {
					if ($orgWidth > $orgHeight) {
						$args["width"] = $args["max"];
					}
					else {
						$args["height"] = $args["max"];
					}
					unset($args["max"]);
				}
				// Only width
				if (array_key_exists("width", $args)) {
					$size = $this->calculateAspectResize($image, $args["width"], null);
				}
				// Only height
				else if (array_key_exists("height", $args)) {
					$size = $this->calculateAspectResize($image, null, $args["height"]);
				}
				// Set size from calculated aspect resize
				$width = $size[0];
				$height = $size[1];
			}
			// Double args
			else if (count($args) == 2) {
				if (array_key_exists("width", $args) && array_key_exists("height", $args)) {
					$width = $args["width"];
					$height = $args["height"];
				}
				else if (array_key_exists(0, $args) && array_key_exists(1, $args)) {
					$width = $args[0];
					$height = $args[1];
				}
			}
		}
		// Scale
		$image->scale($width, $height);
	}
	
	/**
	 * Calculates the target size in order to maintain aspect ratio.
	 * @param \KojinExt\ImageHandler\Image $image
	 * @param int $width
	 * @param int $height
	 * @return array of width and height values
	 */
	public function calculateAspectResize(Image $image, $width = null, $height = null) {
		// Both are supplied
		if ($width !== null && $height !== null) {
			return array($width, $height);
		}
		// Get image size
		$orgWidth = $image->getWidth();
		$orgHeight = $image->getHeight();
		// Aspect ratio
		$aspect = $orgWidth / $orgHeight;
		// Neither are supplied
		if ($width === null && $height === null) {
			return array($orgWidth, $orgHeight);
		}
		// Only width - auto height
		if ($width !== null && $height === null) {
			return array(
				$width,
				round($width / $aspect)
			);
		}
		// Only height - auto width
		if ($width === null && $height !== null) {
			return array(
				round($height * $aspect),
				$height
			);
		}

		throw new ImageException("This should never happen");
	}
}
