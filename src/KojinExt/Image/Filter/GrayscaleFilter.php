<?php

namespace KojinExt\Image\Filter;

use KojinExt\Image\ImageException;
use KojinExt\Image\Image;

/**
 * Filter used for grayscale images.
 */
class GrayscaleFilter implements ImageFilter {
	public function execute(Image $image, array $args = null) {
		// No arguments
		if (is_null($args) === false) {
			throw new ImageException("Grayscale filter does not accept any arguments.");
		}
		// Execute
		//$image->getImagick()->setColorspace(\Imagick::COLORSPACE_GRAY);
		//$image->getImagick()->modulateImage(100, 0, 100);

		// Custom algorithm
		$iterator = $image->getImagick()->getPixelIterator();
		foreach ($iterator as $row => $pixels) {
			foreach ($pixels as $column => $pixel) {
				// Extract color
				$r = $pixel->getColorValue(\Imagick::COLOR_RED);
				$g = $pixel->getColorValue(\Imagick::COLOR_GREEN);
				$b = $pixel->getColorValue(\Imagick::COLOR_BLUE);
				// Convert to grayscale while preserving luminance
				// http://docs.gimp.org/2.6/en/gimp-tool-desaturate.html
				$gray = $r * 0.21 + $g * 0.72 + $b * 0.07;
				// Set color
				$pixel->setColorValue(\Imagick::COLOR_RED,   $gray);
				$pixel->setColorValue(\Imagick::COLOR_GREEN, $gray);
				$pixel->setColorValue(\Imagick::COLOR_BLUE,  $gray);
			}
			$iterator->syncIterator();
		}
	}
}
