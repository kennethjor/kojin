<?php

namespace KojinExt\Image\Filter;

use KojinExt\Image\Image;

interface ImageFilter {
	public function execute(Image $image, array $args = null);
}
