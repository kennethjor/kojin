<?php

namespace KojinExt\Image\Filter;

use KojinExt\Image\ImageException;
use KojinExt\Image\Image;

/**
 * Filter used for cropping images.
 */
class CropFilter implements ImageFilter {
	public function execute(Image $image, array $args = null) {
		// Arguments must be (width, height, top, left)
		if (is_null($args)) {
			throw new ImageHandlerException("No arguments supplied to crop filter");
		}
		if (count($args) != 4) {
			throw new ImageHandlerException("Exactly 4 arguments expected in crop filter");
		}
		foreach ($args as $k => $v) {
			if (is_integer($k) === false) {
				throw new ImageHandlerException("Named arguments not supported for crop filter");
			}
		}
		// Import values
		$imageSize = array($image->getWidth(), $image->getHeight());
		$cropSize = array($args[0], $args[1]);
		$position = array($args[2], $args[3]);
		// Convert string positions @todo if the words are unique, order could not matter
		if ($position[0] === "left")   $position[0] = 0.0;
		if ($position[0] === "center") $position[0] = 0.5;
		if ($position[0] === "right")  $position[0] = 1.0;
		if ($position[1] === "top")    $position[1] = 0.0;
		if ($position[1] === "middle") $position[1] = 0.5;
		if ($position[1] === "bottom") $position[1] = 1.0;
		// Validate positions
		if (is_string($position[0])) {
			throw new ImageHandlerException("Invalid position string in crop filter: " . $position[0]);
		}
		if (is_string($position[1])) {
			throw new ImageHandlerException("Invalid position string in crop filter: " . $position[1]);
		}
		// Calculate crop
		$crop = $this->calculateCropCoordinates($imageSize, $cropSize, $position);
		// Crop
		$image->crop($crop[0], $crop[1], $crop[2], $crop[3]);
	}

	/**
	 * Calculates the final crop coordinates based on desired paramters.
	 * @param array $iamgeSize The size of the original image.
	 * @param array $cropSize The desired final cropped size.
	 * @param array $position The relative position in x and y of the cropped image inside the main image.
	 * @return array of width, height, top, and left coordinated for direct use in cropping.
	 */
	public function calculateCropCoordinates($imageSize, $cropSize, $position) {
		// Limit position between 0 and 1
		$position[0] = max(0, min(1, $position[0]));
		$position[1] = max(0, min(1, $position[1]));
		// Limit crop size inside image size
		$cropSize[0] = min($cropSize[0], $imageSize[0]);
		$cropSize[1] = min($cropSize[1], $imageSize[1]);
		// Calculate offsets based on slack space and position
		$offsetX = ($imageSize[0] - $cropSize[0]) * $position[0];
		$offsetY = ($imageSize[1] - $cropSize[1]) * $position[1];
		// Done
		return array(
			intval(round($cropSize[1])),
			intval(round($cropSize[0])),
			intval(round($offsetX)),
			intval(round($offsetY))
		);
	}
}
