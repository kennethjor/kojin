<?php

namespace KojinExt\Markdown;

use Kojin\Extension\Extension;
use Kojin\Extension\PageLoader;
use Kojin\Fs\File;
use Kojin\Kojin;
use Kojin\Route\FileTypeRoute;

/**
 * Markdown extension.
 */
class MarkdownExtension extends Extension implements PageLoader {
	private static $parser;

	/**
	 * Load event.
	 * @return void
	 */
	public static function onLoad() {
		// Load PHP markdown
		require_once(dirname(__FILE__) . DS . "php-markdown" . DS . "markdown.php");
		static::$parser = new \Markdown_Parser();
	}

	/**
	 * Init event.
	 */
	public function onInit(Kojin $kojin) {
		// Register routes
		$mimetype = "text/x-markdown";
		$route = new FileTypeRoute();
		$route
			->addFileType("md",       $mimetype)
			->addFileType("markdown", $mimetype);
		$kojin->getRouter()->addRoute("markdown", $route, "markdown");
	}
	
	/**
	 * Loads content from a file.
	 * @param \Kojin\Fs\File $file
	 * @return string html
	 */
	public function loadFile(File $file) {
		return $this->loadSource($file->getContents());
	}

	/**
	 * Loads content from source.
	 * @param string $source
	 * @return string html
	 */
	public function loadSource($source) {
		return self::$parser->transform($source);
	}
}
